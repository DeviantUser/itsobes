package com.gmail.borlandlp.domain.pushservice

interface PushCompanyService {
    suspend fun create(): AddPushCompanyResult
    suspend fun put(company: PushCompany): SetPushCompanySettingsResult
    suspend fun get(): GetPushCompanySettingsResult
    suspend fun remove(pushCompanyId: Int): RemovePushCompanySettingsResult
    suspend fun switchNotificationsState(newState: Boolean): SwitchPushCompanyStateResult
}

class PushCompany(
    val id: Int,
    val state: Boolean,
    val hours: Int,
    val minutes: Int,
    val daysState: Map<DayOfWeek, Boolean>,
)

sealed interface AddPushCompanyResult {
    class Success(
        val data: PushCompany,
    ): AddPushCompanyResult

    class Error(
        val message: String?,
    ): AddPushCompanyResult

    data object NoConnectionError : AddPushCompanyResult
}

sealed interface GetPushCompanySettingsResult {
    class Success(
        val data: List<PushCompany>,
        val enabled: Boolean,
    ): GetPushCompanySettingsResult

    class Error(
        val message: String?,
    ): GetPushCompanySettingsResult

    data object NoConnectionError : GetPushCompanySettingsResult
}

sealed interface SetPushCompanySettingsResult {
    data object Success : SetPushCompanySettingsResult

    class Error(
        val message: String?,
    ): SetPushCompanySettingsResult

    data object NoConnectionError : SetPushCompanySettingsResult
}

sealed interface RemovePushCompanySettingsResult {
    data object Success : RemovePushCompanySettingsResult

    class Error(
        val message: String?,
    ): RemovePushCompanySettingsResult

    data object NoConnectionError : RemovePushCompanySettingsResult
}

sealed interface SwitchPushCompanyStateResult {
    data object Success : SwitchPushCompanyStateResult

    class Error(
        val message: String?,
    ): SwitchPushCompanyStateResult
}
