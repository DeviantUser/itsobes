package com.gmail.borlandlp.domain.bookmarkquestion

import com.gmail.borlandlp.domain.answer.AnswersRepository
import com.gmail.borlandlp.domain.question.QuestionsRepository
import javax.inject.Inject

class GetBookmarkedQuestionInteractor @Inject constructor(
    private val answersRepository: AnswersRepository,
    private val questionRepository: QuestionsRepository,
) {
    suspend fun execute(questionLink: String): BookmarkQuestionFullInfo {
        val question = questionRepository.get(questionLink)
        val answer = answersRepository.getBy(questionLink)
        return BookmarkQuestionFullInfo(
            title = question!!.title,
            tags = answer!!.tags,
            content = answer.content,
            link = question.linkToSource,
        )
    }
}