package com.gmail.borlandlp.domain.quizpreferred

data class UserPreferSettings(
    val questionsCount: Int,
    val tags: List<QuizPreferredTag>,
)