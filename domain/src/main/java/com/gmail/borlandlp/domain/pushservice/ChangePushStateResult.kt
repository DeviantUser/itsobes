package com.gmail.borlandlp.domain.pushservice

sealed interface ChangePushStateResult {
    data object RequestedEnableNotifications : ChangePushStateResult
    class Success(val newState: Boolean) : ChangePushStateResult
    class Error(val message: String? = null) : ChangePushStateResult
}