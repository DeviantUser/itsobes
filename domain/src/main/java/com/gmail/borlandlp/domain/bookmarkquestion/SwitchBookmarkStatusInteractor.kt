package com.gmail.borlandlp.domain.bookmarkquestion

import javax.inject.Inject

class SwitchBookmarkStatusInteractor @Inject constructor(
    private val bookmarkRepository: BookmarkQuestionRepository,
) {
    suspend fun execute(link: String): SwitchBookmarkResult {
        val currentBookmark = bookmarkRepository.get(link)
        if (currentBookmark != null) {
            bookmarkRepository.remove(currentBookmark)
            return SwitchBookmarkResult.BookmarkIsRemoved
        } else {
            bookmarkRepository.add(link)
            return SwitchBookmarkResult.AddedToBookmark
        }
    }
}

sealed interface SwitchBookmarkResult {
    data object AddedToBookmark : SwitchBookmarkResult
    data object BookmarkIsRemoved : SwitchBookmarkResult
    data class Error(
        val message: String?,
    ) : SwitchBookmarkResult
}