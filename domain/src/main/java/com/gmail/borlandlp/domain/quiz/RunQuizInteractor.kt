package com.gmail.borlandlp.domain.quiz

import com.gmail.borlandlp.domain.dislikequiz.DislikedQuestionRepository
import com.gmail.borlandlp.domain.question.Question
import com.gmail.borlandlp.domain.question.QuestionTag
import com.gmail.borlandlp.domain.question.QuestionsRepository
import javax.inject.Inject
import kotlin.random.Random

class RunQuizInteractor @Inject constructor(
    private val questionsRepository: QuestionsRepository,
    private val quizRepository: QuizRepository,
    private val dislikedQuestionsRepository: DislikedQuestionRepository,
) {
    suspend fun execute(count: Int, tags: List<QuestionTag>): Result {
        // TODO Вынести в отдельный валидатор?
        if (count <= 0) {
            return Result.Error(message = "Ошибка. Число вопросов должно быть больше нуля")
        } else if (tags.isEmpty()) {
            return Result.Error(message = "Ошибка. Должен быть выбран хотя бы один тег")
        }

        val questions = questionsRepository.getAll(tags = tags)
        val randomizedQuestions = getRandom(count, questions)
        val quiz = quizRepository.add(Quiz(
            id = 0,
            done = false,
            tags = tags.map { it.id },
            questionsLinks = randomizedQuestions.map { it.linkToSource },
            createdUnixTime = System.currentTimeMillis() / 1000,
        ))

        // reg questions in db
        return Result.Success(quiz = quiz)
    }

    private suspend fun getRandom(count: Int, questions: List<Question>): List<Question> {
        if (questions.size <= count) {
            return questions.shuffled()
        }
        val blockQuestions = dislikedQuestionsRepository.getAll() // Заменить на sql join?

        val random = Random(Int.MAX_VALUE + System.currentTimeMillis())
        val generatedQuestions = mutableMapOf<Int, Question>()
        // TODO можно переписать отсеивание заблокированных вопросов до меньшей временной сложности
        while (generatedQuestions.size < count) {
            val randomNumber = random.nextInt(questions.size-1)
            if (!generatedQuestions.containsKey(randomNumber) && !blockQuestions.contains(questions[randomNumber].linkToSource)) {
                generatedQuestions[randomNumber] = questions[randomNumber]
            }
        }

        return generatedQuestions.values.toList()
    }

    sealed interface Result {
        data class Success(
            val quiz: Quiz,
        ) : Result
        class Error(
            val message: String?,
        ) : Result
    }
}
