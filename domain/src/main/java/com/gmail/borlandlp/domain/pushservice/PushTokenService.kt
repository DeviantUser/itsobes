package com.gmail.borlandlp.domain.pushservice

interface PushTokenService {
    suspend fun set(pushToken: String): SetTokenResult
    suspend fun get(): GetTokenResult
    suspend fun getInternalPushToken(): String
}

sealed interface SetTokenResult {
    data object Success : SetTokenResult
    class Error(val message: String?) : SetTokenResult
    data object NetworkException : SetTokenResult
}

sealed interface GetTokenResult {
    class Success(
        val token: String,
    ): GetTokenResult
    class Error(val message: String?) : GetTokenResult
    data object NetworkException : GetTokenResult
}
