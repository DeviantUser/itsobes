package com.gmail.borlandlp.domain.pushservice

import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

class DisablePushNotificationsUseCase @Inject constructor(
    private val pushCompanyService: PushCompanyService,
) {
    operator fun invoke(): Flow<ChangePushStateResult> {
        return flow {
            when (pushCompanyService.switchNotificationsState(newState = false)) {
                is SwitchPushCompanyStateResult.Success -> {
                    emit(ChangePushStateResult.Success(newState = false))
                }
                is SwitchPushCompanyStateResult.Error -> {
                    emit(ChangePushStateResult.Error())
                }
            }
        }
    }
}