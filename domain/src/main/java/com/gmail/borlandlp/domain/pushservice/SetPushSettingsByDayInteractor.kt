package com.gmail.borlandlp.domain.pushservice

import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

class SetPushSettingsByDayInteractor @Inject constructor(
    private val pushSettingsService: PushSettingsService,
    private val pushTokenService: PushTokenService,
) {
    operator fun invoke(preference: PushPreference): Flow<SetPushSettingsResult> = flow {
        val devicePushToken = pushTokenService.getInternalPushToken()

        if (devicePushToken.isEmpty()) {
            emit(SetPushSettingsResult.Error(message = "Ошибка при получении push-токена"))
            return@flow
        }

        val serverPushTokenResult = pushTokenService.get()
        if (serverPushTokenResult !is GetTokenResult.Success) {
            emit(SetPushSettingsResult.Error(message = "Ошибка при получении push-токена с сервера"))
            return@flow
        }

        val serverToken = serverPushTokenResult.token
        if (devicePushToken != serverToken) {
            val tokenResponse = pushTokenService.set(devicePushToken)
            if (tokenResponse is SetTokenResult.Error) {
                emit(SetPushSettingsResult.Error(message = tokenResponse.message ?: "Ошибка при отправке push-токена на сервер"))
                return@flow
            }
        }
        emit(pushSettingsService.set(preference = preference))
    }
}