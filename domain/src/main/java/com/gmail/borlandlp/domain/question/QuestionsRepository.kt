package com.gmail.borlandlp.domain.question

interface QuestionsRepository {
    suspend fun getAll(
        tags: List<QuestionTag>? = null,
    ): List<Question>

    suspend fun get(linkUrl: String): Question?
}