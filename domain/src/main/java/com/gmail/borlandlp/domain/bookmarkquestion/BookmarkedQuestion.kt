package com.gmail.borlandlp.domain.bookmarkquestion

class BookmarkedQuestion(
    val link: String,
    val markUnixTime: Long,
)