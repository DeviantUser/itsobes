package com.gmail.borlandlp.domain.question

class Question (
    val linkToSource: String,
    val tags: List<QuestionTag>,
    val title: String,
)

enum class QuestionTag(
    val id: String,
    val title: String,
) {
    JAVA(id = "JAVA", title = "Java"),
    ANDROID(id = "ANDROID", title = "Android"),
    IT(id = "IT", title = "IT"),
    DESIGN_PATTERN(id = "DESIGN_PATTERN", title = "Design patterns");
}