package com.gmail.borlandlp.domain.quiz

data class QuestionUiState (
    val question: String = "",
    val answer: String = "",
    val isLast: Boolean = false,
    val linkToSource: String = "",
    /* Какой вопрос по счету */
    val currentQuestionIndex: Int = 0,
    /* Всего вопросов */
    val totalQuestions: Int = 0,
    val tags: List<String> = emptyList(),
)