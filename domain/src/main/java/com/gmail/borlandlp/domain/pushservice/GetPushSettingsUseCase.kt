package com.gmail.borlandlp.domain.pushservice

import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

class GetPushSettingsUseCase @Inject constructor(
    private val pushCompanyService: PushCompanyService,
    private val localPushService: LocalPushService,
) {
    operator fun invoke(): Flow<GetSettingsResult> = flow {
        val result = pushCompanyService.get()
        if (result is GetPushCompanySettingsResult.Success) {
            emit(
                GetSettingsResult.Success(
                    enabled = localPushService.notificationsAreEnabled() && result.enabled,
                    data = result.data,
                )
            )
        } else if (result is GetPushCompanySettingsResult.Error) {
            emit(GetSettingsResult.Error(message = result.message))
        } else {
            emit(GetSettingsResult.NoConnectionError)
        }
    }
}

sealed interface GetSettingsResult {
    class Success(
        val data: List<PushCompany>,
        val enabled: Boolean,
    ): GetSettingsResult

    class Error(
        val message: String?,
    ): GetSettingsResult

    data object NoConnectionError : GetSettingsResult
}
