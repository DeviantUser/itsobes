package com.gmail.borlandlp.domain.quizpreferred

import com.gmail.borlandlp.domain.question.QuestionTag
import javax.inject.Inject

class GetPreferQuizSettingsInteractor @Inject constructor(
    private val storage: PreferredQuizSettingsStorage,
) {
    // TODO Должен возвращать sealed interface
    suspend fun execute(): UserPreferSettings {
        val settings = storage.get()
        if (settings == null) {
            return UserPreferSettings(
                questionsCount = 10,
                tags = QuestionTag.entries.map {
                    QuizPreferredTag(id = it.id, title = it.title, checked = true)
                },
            )
        } else {
            val tags = QuestionTag.entries.map {
                QuizPreferredTag(
                    id = it.id,
                    title = it.title,
                    checked = settings.tags.contains(it),
                )
            }

            return UserPreferSettings(
                questionsCount = settings.questionsCount,
                tags = tags,
            )
        }
    }
}