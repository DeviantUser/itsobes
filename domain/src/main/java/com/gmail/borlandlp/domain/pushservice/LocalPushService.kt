package com.gmail.borlandlp.domain.pushservice

interface LocalPushService {
    fun notificationsAreEnabled(): Boolean
    fun requestEnableNotifications()
}