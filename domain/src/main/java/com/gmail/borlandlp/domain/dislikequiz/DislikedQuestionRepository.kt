package com.gmail.borlandlp.domain.dislikequiz

interface DislikedQuestionRepository {
    /**
     * @return Есть ли у квиза дизлайк
     */
    suspend fun isDisliked(questionLink: String): Boolean

    /**
     * @return Список квизов с дизлайками
     */
    suspend fun getDisliked(questionLink: List<String>): List<String>

    /**
     * @return Вернуть список всех дизлайков
     */
    suspend fun getAll(): List<String>

    /**
     * @return Установить дизлайк
     */
    suspend fun addDislike(questionLink: String)
    suspend fun removeDislike(questionLink: String)
}