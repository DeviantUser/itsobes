package com.gmail.borlandlp.domain.quiz

data class Quiz (
    val id: Int,
    val done: Boolean,
    val tags: List<String>,
    val questionsLinks: List<String>,
    val createdUnixTime: Long,
)