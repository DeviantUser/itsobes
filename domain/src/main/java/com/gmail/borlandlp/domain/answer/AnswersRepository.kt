package com.gmail.borlandlp.domain.answer

interface AnswersRepository {
    suspend fun getBy(linkToSource: String): Answer?
}