package com.gmail.borlandlp.domain.bookmarkquestion

import com.gmail.borlandlp.domain.question.Question
import com.gmail.borlandlp.domain.question.QuestionsRepository
import javax.inject.Inject

class GetBookmarksListInteractor @Inject constructor(
    private val bookmarkQuestionRepository: BookmarkQuestionRepository,
    private val questionRepository: QuestionsRepository,
) {
    suspend fun execute(): List<BookmarkQuestionInfo> {
        val bookmarksList = bookmarkQuestionRepository.getAll()
        val questions = mutableListOf<Question>()
        bookmarksList.forEach {
            val question = questionRepository.get(it.link)
            if (question != null) {
                questions.add(question)
            }
        }

        return questions.toBookmarkQuestionInfo()
    }
}

private fun MutableList<Question>.toBookmarkQuestionInfo(): List<BookmarkQuestionInfo> {
    return this.map {
        BookmarkQuestionInfo(
            link = it.linkToSource,
            title = it.title,
            addUnixTime = 0,
        )
    }
}
