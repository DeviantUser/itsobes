package com.gmail.borlandlp.domain.quizpreferred

import com.gmail.borlandlp.domain.question.QuestionTag
import javax.inject.Inject

class SetPreferQuizSettingsInteractor @Inject constructor(
    private val storage: PreferredQuizSettingsStorage,
) {
    suspend fun execute(settings: UserPreferSettings): Result {
        // TODO Вынести в отдельный валидатор?
        if (settings.questionsCount <= 0) {
            return Result.Error(message = "Ошибка. Число вопросов должно быть больше нуля")
        } else if (settings.tags.isEmpty()) {
            return Result.Error(message = "Ошибка. Должен быть выбран хотя бы один тег")
        }

        storage.set(settings.toPreferredSettings())

        return Result.Success
    }

    sealed interface Result {
        data object Success : Result
        class Error(
            val message: String?,
        ) : Result
    }
}

private fun UserPreferSettings.toPreferredSettings(): PreferredSettings {
    return PreferredSettings(
        questionsCount = questionsCount,
        tags = tags.filter { it.checked }.map { QuestionTag.valueOf(it.id) },
    )
}
