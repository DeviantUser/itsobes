package com.gmail.borlandlp.domain.quiz

import com.gmail.borlandlp.domain.answer.Answer
import com.gmail.borlandlp.domain.answer.AnswersRepository
import com.gmail.borlandlp.domain.question.QuestionsRepository
import javax.inject.Inject

class GetAnswerInteractor @Inject constructor(
    private val answersRepository: AnswersRepository,
    private val quizRepository: QuizRepository,
    private val questionRepository: QuestionsRepository,
) {
    suspend fun execute(quizId: Int, questionPos: Int): QuestionUiState {
        val quiz = quizRepository.getById(quizId)

        val answerId = quiz.questionsLinks[questionPos] ?: quiz.questionsLinks.first()

        val answer = answersRepository.getBy(answerId)!!
        val currentQuestionIndex = quiz.questionsLinks.indexOf(answerId)
        return answer.toUiState(
            isLast = currentQuestionIndex >= quiz.questionsLinks.lastIndex,
            currentIndex = currentQuestionIndex,
            totalQuestions = quiz.questionsLinks.size,
            title = questionRepository.get(answerId)!!.title,
        )
    }
}

private fun Answer.toUiState(
    isLast: Boolean,
    currentIndex: Int,
    totalQuestions: Int,
    title: String,
): QuestionUiState {
    return QuestionUiState(
        question = title,
        answer = this.content,
        isLast = isLast,
        linkToSource = this.linkToSource,
        totalQuestions = totalQuestions,
        currentQuestionIndex = currentIndex,
        tags = this.tags,
    )
}