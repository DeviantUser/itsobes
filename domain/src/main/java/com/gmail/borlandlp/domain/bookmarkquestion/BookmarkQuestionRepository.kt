package com.gmail.borlandlp.domain.bookmarkquestion

interface BookmarkQuestionRepository {
    suspend fun add(link: String)
    suspend fun get(link: String): BookmarkedQuestion?
    suspend fun getAll(): List<BookmarkedQuestion>
    suspend fun remove(bookmark: BookmarkedQuestion)
}