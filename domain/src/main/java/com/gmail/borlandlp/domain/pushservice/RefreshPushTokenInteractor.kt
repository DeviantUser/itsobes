package com.gmail.borlandlp.domain.pushservice

import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

class RefreshPushTokenInteractor @Inject constructor(
    private val pushTokenService: PushTokenService,
) {
    operator fun invoke(): Flow<RefreshResult> = flow {
        val devicePushToken = pushTokenService.getInternalPushToken()
        if (devicePushToken.isEmpty()) {
            emit(RefreshResult.Error)
            return@flow
        }

        val tokenResponse = pushTokenService.set(devicePushToken)
        if (tokenResponse !is SetTokenResult.Success) {
            emit(RefreshResult.Error)
            return@flow
        } else {
            emit(RefreshResult.Success)
        }
    }
}

sealed interface RefreshResult {
    data object Success : RefreshResult
    data object Error : RefreshResult
}
