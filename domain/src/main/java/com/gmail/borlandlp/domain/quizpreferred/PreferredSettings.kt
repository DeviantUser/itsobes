package com.gmail.borlandlp.domain.quizpreferred

import com.gmail.borlandlp.domain.question.QuestionTag

data class PreferredSettings(
    val questionsCount: Int,
    val tags: List<QuestionTag>,
)
