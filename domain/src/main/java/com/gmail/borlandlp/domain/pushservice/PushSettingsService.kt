package com.gmail.borlandlp.domain.pushservice

@Deprecated("use PushCompanyService")
interface PushSettingsService {
    suspend fun set(preference: PushPreference): SetPushSettingsResult
    suspend fun get(): GetPushSettingsResult
    suspend fun remove(dayOfWeek: DayOfWeek): RemovePushSettingsResult
}

sealed interface GetPushSettingsResult {
    class Success(
        val data: List<PushPreference>,
    ): GetPushSettingsResult

    class Error(
        val message: String?,
    ): GetPushSettingsResult

    data object NoConnectionError : GetPushSettingsResult
}

sealed interface SetPushSettingsResult {
    data object Success : SetPushSettingsResult

    class Error(
        val message: String?,
    ): SetPushSettingsResult

    data object NoConnectionError : SetPushSettingsResult
}

sealed interface RemovePushSettingsResult {
    data object Success : RemovePushSettingsResult

    class Error(
        val message: String?,
    ): RemovePushSettingsResult

    data object NoConnectionError : RemovePushSettingsResult
}
