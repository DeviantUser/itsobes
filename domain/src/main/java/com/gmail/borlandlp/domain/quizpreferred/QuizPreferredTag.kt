package com.gmail.borlandlp.domain.quizpreferred

class QuizPreferredTag(
    val id: String,
    val title: String,
    var checked: Boolean,
)