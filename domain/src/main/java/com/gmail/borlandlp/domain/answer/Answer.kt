package com.gmail.borlandlp.domain.answer

class Answer(
    val linkToSource: String,
    val tags: List<String>,
    val content: String,
)