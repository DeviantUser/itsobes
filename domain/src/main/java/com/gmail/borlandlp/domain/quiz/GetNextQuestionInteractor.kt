package com.gmail.borlandlp.domain.quiz

import javax.inject.Inject

class GetNextQuestionInteractor @Inject constructor (
    private val quizRepository: QuizRepository,
) {
    suspend fun execute(quizId: Int, currentQuestionLink: String): String? {
        val quiz = quizRepository.getById(quizId)
        val currentIndex = quiz.questionsLinks.indexOf(currentQuestionLink)
        return if (currentIndex >= 0 && currentIndex < quiz.questionsLinks.lastIndex) {
            quiz.questionsLinks[currentIndex+1]
        } else {
            null
        }
    }
}