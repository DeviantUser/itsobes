package com.gmail.borlandlp.domain.quizpreferred

interface PreferredQuizSettingsStorage {
    suspend fun get(): PreferredSettings?
    suspend fun set(entity: PreferredSettings)
}