package com.gmail.borlandlp.domain.pushservice

class PushPreference(
    val state: State,
    val dayOfWeek: DayOfWeek,
    val hours: Int,
    val minutes: Int,
) {
    enum class State {
        ON,
        OFF,
        DISABLED,
    }
}