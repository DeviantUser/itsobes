package com.gmail.borlandlp.domain.bookmarkquestion

class BookmarkQuestionInfo(
    val link: String,
    val title: String,
    val addUnixTime: Long,
)