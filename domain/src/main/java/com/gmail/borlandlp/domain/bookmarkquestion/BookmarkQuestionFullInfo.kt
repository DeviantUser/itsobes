package com.gmail.borlandlp.domain.bookmarkquestion

class BookmarkQuestionFullInfo(
    val title: String,
    val tags: List<String>,
    val content: String,
    val link: String,
)