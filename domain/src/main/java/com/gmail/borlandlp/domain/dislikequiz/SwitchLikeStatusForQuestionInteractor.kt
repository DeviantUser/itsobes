package com.gmail.borlandlp.domain.dislikequiz

import javax.inject.Inject

class SwitchLikeStatusForQuestionInteractor @Inject constructor(
    private val repository: DislikedQuestionRepository,
) {
    suspend fun execute(quizLink: String): SwitchResult {
        return if (repository.isDisliked(quizLink)) {
            repository.removeDislike(quizLink)
            SwitchResult.Liked
        } else {
            repository.addDislike(quizLink)
            SwitchResult.Disliked
        }
    }
}

sealed interface SwitchResult {
    data object Liked : SwitchResult
    data object Disliked : SwitchResult
    data class Error(
        val message: String?,
    ) : SwitchResult
}