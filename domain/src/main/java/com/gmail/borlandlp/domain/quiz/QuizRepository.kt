package com.gmail.borlandlp.domain.quiz

interface QuizRepository {
    suspend fun add(quiz: Quiz): Quiz
    suspend fun remove(id: Int)
    suspend fun save(quiz: Quiz)
    suspend fun get(byIds: List<Int>): List<Quiz>
    suspend fun getById(id: Int): Quiz
}