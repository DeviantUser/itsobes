package com.gmail.borlandlp.domain.pushservice

import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

class EnablePushNotificationsUseCase @Inject constructor(
    private val localPushService: LocalPushService,
    private val pushCompanyService: PushCompanyService,
    private val pushTokenService: PushTokenService,
) {
    operator fun invoke(): Flow<ChangePushStateResult> {
        return flow {
            if (!localPushService.notificationsAreEnabled()) {
                localPushService.requestEnableNotifications()

                emit(ChangePushStateResult.RequestedEnableNotifications)
                return@flow
            }

            when (pushCompanyService.switchNotificationsState(newState = true)) {
                is SwitchPushCompanyStateResult.Success -> {
                }
                is SwitchPushCompanyStateResult.Error -> {
                    emit(ChangePushStateResult.Error(
                        message = "Возникла ошибка при включении пуш-уведмолений",
                    ))
                    return@flow
                }
            }

            val devicePushToken = pushTokenService.getInternalPushToken()
            if (devicePushToken.isEmpty()) {
                emit(ChangePushStateResult.Error(
                    message = "Возникла ошибка при получении пуш-токена",
                ))
                return@flow
            }

            val tokenResponse = pushTokenService.set(devicePushToken)
            if (tokenResponse !is SetTokenResult.Success) {
                emit(ChangePushStateResult.Error(
                    message = "Возникла ошибка при отправке пуш-токена",
                ))
                return@flow
            } else {
                emit(ChangePushStateResult.Success(
                    newState = true,
                ))
            }
        }
    }
}