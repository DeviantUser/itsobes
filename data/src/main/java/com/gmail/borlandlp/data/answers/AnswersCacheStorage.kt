package com.gmail.borlandlp.data.answers

class AnswersCacheStorage(
    private val answersDao: CacheAnswersDao,
    private val remoteSource: AnswersSource,
) : AnswersSource {
    override suspend fun getByLink(link: String): AnswerData? {
        val cachedItem = answersDao.getAnswerFor(link)
        if (cachedItem != null) {
            return cachedItem.toAnswerData()
        }

        val question = remoteSource.getByLink(link)
        if (question != null) {
            answersDao.insertAll(listOf(question.toDbEntity()))
        }

        return question
    }

    override fun isCanHandleLink(link: String): Boolean {
        return remoteSource.isCanHandleLink(link)
    }
}

private fun QuestionAnswerEntity.toAnswerData(): AnswerData {
    return AnswerData(
        link = this.link,
        answerCustomTags = this.tags,
        content = this.content,
    )
}

private fun AnswerData.toDbEntity(): QuestionAnswerEntity {
    return QuestionAnswerEntity(
        link = this.link,
        tags = this.answerCustomTags,
        content = this.content,
    )
}
