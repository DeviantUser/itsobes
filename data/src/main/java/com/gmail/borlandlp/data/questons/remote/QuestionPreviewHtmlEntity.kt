package com.gmail.borlandlp.data.questons.remote

data class QuestionPreviewHtmlEntity (
    val title: String,
    val link: String,
    val tags: List<String>,
)