package com.gmail.borlandlp.data.dislikequiz.local

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query

@Dao
interface DislikeQuestionDao {
    @Query("SELECT * FROM dislikequestionentity WHERE link IN (:links)")
    fun getDisliked(links: List<String>): List<DislikeQuestionEntity>

    @Query("SELECT * FROM dislikequestionentity")
    fun getAll(): List<DislikeQuestionEntity>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(answers: DislikeQuestionEntity)

    @Query("DELETE FROM dislikequestionentity WHERE link IN (:list)")
    fun delete(list: List<String>)
}