package com.gmail.borlandlp.data.pushservice

import com.gmail.borlandlp.data.DeviceContext
import com.gmail.borlandlp.domain.pushservice.DayOfWeek
import com.gmail.borlandlp.domain.pushservice.GetPushSettingsResult
import com.gmail.borlandlp.domain.pushservice.PushPreference
import com.gmail.borlandlp.domain.pushservice.PushSettingsService
import com.gmail.borlandlp.domain.pushservice.RemovePushSettingsResult
import com.gmail.borlandlp.domain.pushservice.SetPushSettingsResult
import java.util.Locale
import javax.inject.Inject

class ImplPushSettingsService @Inject constructor(
    private val api: PushSettingsApi,
    private val deviceContext: DeviceContext,
) : PushSettingsService {
    override suspend fun set(preference: PushPreference): SetPushSettingsResult {
        return try {
            api.setPushSettings(
                deviceId = deviceContext.getDeviceId(),
                dayOfWeek = preference.dayOfWeek.name,
                hours = preference.hours,
                minutes = preference.minutes,
                state = preference.state == PushPreference.State.ON,
            ).toResult()
        } catch (e: Exception) {
            SetPushSettingsResult.NoConnectionError
        }
    }

    override suspend fun get(): GetPushSettingsResult {
        return try {
            api.getPushSettings(
                deviceId = deviceContext.getDeviceId(),
            ).toResult()
        } catch (e: Exception) {
            GetPushSettingsResult.NoConnectionError
        }
    }

    override suspend fun remove(dayOfWeek: DayOfWeek): RemovePushSettingsResult {
        return try {
            api.deletePushSettings(
                deviceId = deviceContext.getDeviceId(),
                dayOfWeek = dayOfWeek.name.lowercase(),
            ).toResult()
        } catch (e: Exception) {
            RemovePushSettingsResult.NoConnectionError
        }
    }
}

private fun GetPushSettingsResponse.toResult(): GetPushSettingsResult {
    return if (this.statusCode == 200) {
        GetPushSettingsResult.Success(
            data = this.data.toPushPreference(),
        )
    } else {
        GetPushSettingsResult.Error(
            message = this.errorMessage,
        )
    }
}

fun DeletePushSettingsResponse.toResult(): RemovePushSettingsResult {
    return if (this.statusCode == 200) {
        RemovePushSettingsResult.Success
    } else {
        RemovePushSettingsResult.Error(
            message = this.errorMessage,
        )
    }
}

private fun List<Settings>.toPushPreference(): List<PushPreference> {
    return this.map {
        PushPreference(
            state = if (it.state) PushPreference.State.ON else PushPreference.State.OFF,
            dayOfWeek = DayOfWeek.valueOf(it.dayOfWeek.uppercase(Locale.getDefault())),
            hours = it.hours,
            minutes = it.minutes,
        )
    }
}

private fun SetPushSettingsResponse.toResult(): SetPushSettingsResult {
    return if(this.statusCode == 200) {
        SetPushSettingsResult.Success
    } else {
        SetPushSettingsResult.Error(this.errorMessage)
    }
}
