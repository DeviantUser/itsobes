package com.gmail.borlandlp.data.questons

import com.gmail.borlandlp.domain.question.QuestionTag

interface QuestionSource {
    suspend fun loadQuestionsByTag(tag: QuestionTag): List<QuestionData>
    fun isCanHandleTag(tag: QuestionTag): Boolean
    fun isCanHandleUrl(url: String): Boolean
    fun loadQuestionByUrl(url: String): QuestionData?
}