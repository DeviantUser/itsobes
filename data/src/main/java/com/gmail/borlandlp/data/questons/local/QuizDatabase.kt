package com.gmail.borlandlp.data.questons.local

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.gmail.borlandlp.data.answers.CacheAnswersDao
import com.gmail.borlandlp.data.answers.QuestionAnswerEntity
import com.gmail.borlandlp.data.bookmarkquestion.db.BookmarkEntity
import com.gmail.borlandlp.data.bookmarkquestion.db.BookmarkQuestionsDao
import com.gmail.borlandlp.data.dislikequiz.local.DislikeQuestionDao
import com.gmail.borlandlp.data.dislikequiz.local.DislikeQuestionEntity
import com.gmail.borlandlp.data.quiz.local.QuizDao
import com.gmail.borlandlp.data.quiz.local.QuizEntity

@Database(
    entities = [
        QuestionDbEntity::class,
        QuestionAnswerEntity::class,
        QuizEntity::class,
        DislikeQuestionEntity::class,
        BookmarkEntity::class,
    ],
    version = 1,
    autoMigrations = [],
    exportSchema = true,
)
@TypeConverters(Converters::class)
abstract class QuizDatabase : RoomDatabase() {
    abstract fun cacheQuestionsDao(): CacheQuestionsDao
    abstract fun getAnswersDao(): CacheAnswersDao
    abstract fun getQuizDao(): QuizDao
    abstract fun getDislikeQuizDao(): DislikeQuestionDao
    abstract fun getBookmarkQuestionsDao(): BookmarkQuestionsDao
}
