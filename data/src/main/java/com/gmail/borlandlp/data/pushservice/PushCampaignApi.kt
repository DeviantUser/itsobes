package com.gmail.borlandlp.data.pushservice

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import retrofit2.http.DELETE
import retrofit2.http.Field
import retrofit2.http.FieldMap
import retrofit2.http.FormUrlEncoded
import retrofit2.http.GET
import retrofit2.http.PATCH
import retrofit2.http.POST
import retrofit2.http.Path

interface PushCampaignApi {
    @POST("push/company")
    @FormUrlEncoded
    suspend fun add(
        @Field("device_id") deviceId: String,
    ): CreateCompanyResponse

    @PATCH("push/company")
    @FormUrlEncoded
    suspend fun put(
        @Field("device_id") deviceId: String,
        @Field("campaign_id") campaignId: Int,
        @Field("state") state: Boolean,
        @Field("hours") hours: Int,
        @Field("minutes") minutes: Int,
        @FieldMap(encoded = false) preferences: Map<String, Boolean>,
    ): StatusResponse

    @GET("push/company/list/{device_id}")
    suspend fun get(
        @Path("device_id") deviceId: String,
    ): GetCompaniesResponse

    @DELETE("push/company/{device_id}/{campaign_id}")
    suspend fun remove(
        @Path("device_id") deviceId: String,
        @Path("campaign_id") campaignId: Int,
    ): StatusResponse

    @PATCH("push/settings/notification")
    @FormUrlEncoded
    suspend fun updatePushNotificationSettings(
        @Field("device_id") deviceId: String,
        @Field("enabled") enabled: Boolean,
    ): StatusResponse
}

@Serializable
class CreateCompanyResponse(
    @SerialName("status_code")
    val statusCode: Int,
    @SerialName("error_message")
    val errorMessage: String? = null,
    @SerialName("data")
    val data: NewCampaignData? = null,
)
@Serializable
class NewCampaignData(
    @SerialName("new_campaign")
    val settings: CompanyInfo,
)

@Serializable
class StatusResponse(
    @SerialName("status_code")
    val statusCode: Int,
    @SerialName("error_message")
    val errorMessage: String? = null,
)

@Serializable
class GetCompaniesResponse(
    @SerialName("status_code")
    val statusCode: Int,
    @SerialName("error_message")
    val errorMessage: String? = null,
    @SerialName("data")
    val data: SettingsData? = null,
)

@Serializable
class SettingsData(
    @SerialName("settings")
    val settings: List<CompanyInfo>,
    @SerialName("notifications_enabled")
    val notificationsAreEnabled: Boolean,
)

@Serializable
class CompanyInfo(
    @SerialName("campaign_id")
    val campaignId: Int,
    val state: Boolean,
    val hours: Int,
    val minutes: Int,
    @SerialName("day_preferences")
    val days: List<DaySettings>,
)

@Serializable
class DaySettings(
    val id: String,
    val state: Boolean,
)