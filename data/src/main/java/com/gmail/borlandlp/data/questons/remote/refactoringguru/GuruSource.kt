package com.gmail.borlandlp.data.questons.remote.refactoringguru

import com.gmail.borlandlp.data.answers.AnswersSource
import com.gmail.borlandlp.data.questons.QuestionData
import com.gmail.borlandlp.data.questons.QuestionSource
import com.gmail.borlandlp.data.questons.remote.QuestionPreviewHtmlEntity
import com.gmail.borlandlp.domain.question.QuestionTag
import org.jsoup.Jsoup
import org.jsoup.nodes.Document
import javax.inject.Inject

class GuruSource @Inject constructor(
    private val mapper: LinkMapper,
) : QuestionSource, AnswersSource {
    override suspend fun loadQuestionsByTag(tag: QuestionTag): List<QuestionData> {
        val link = mapper.getCatalogueLink()
        val doc = Jsoup.connect(link).get()
        val elements = doc.select(".pattern-card")
        val parsedElements = mutableListOf<QuestionPreviewHtmlEntity>()

        elements.iterator().forEach { element ->
            val name = element.select(".pattern-name").text()
            val engName = element.select(".pattern-aka").text()
            parsedElements.add(
                QuestionPreviewHtmlEntity(
                    title = "$name ($engName)",
                    link = mapper.getBaseUrl() + element.attr("href"),
                    tags = listOf(tag.id),
                )
            )
        }

        return parsedElements.toQuestionData()
    }

    override fun isCanHandleTag(tag: QuestionTag): Boolean {
        return SUPPORTED_TAGS.contains(tag)
    }

    override fun isCanHandleUrl(url: String): Boolean {
        return url.contains(mapper.getBaseUrl())
    }

    override fun loadQuestionByUrl(url: String): QuestionData? {
        return null
    }

    override suspend fun getByLink(link: String): com.gmail.borlandlp.data.answers.AnswerData? {
        var doc: Document? = null
        try {
            doc = Jsoup.connect(link).get()
        } catch (_: Exception) {}
        if (doc == null) {
            return null
        }

        val intent = doc.select(".section.intent")
            .html()
            .extractContent()
        val problem = doc.select(".section.problem")
            .html()
            .extractContent()
        val solution = doc.select(".section.solution")
            .html()
            .extractContent()

        return com.gmail.borlandlp.data.answers.AnswerData(
            link = link,
            answerCustomTags = listOf(QuestionTag.DESIGN_PATTERN.id), // TODO refactor
            content = intent + problem + solution,
        )
    }

    private fun String.extractContent(): String {
        return this.replace("/images/patterns", mapper.getBaseUrl() + "/images/patterns")
            .replace("<figure class=\"image\">", "")
            .replace("</figure>", "")
            .replace("width=\"640\"", "width=\"100%\"")
    }

    override fun isCanHandleLink(link: String): Boolean {
        return link.contains("refactoringguru.cn")
    }

    companion object {
        private val SUPPORTED_TAGS = listOf(
            QuestionTag.DESIGN_PATTERN,
        )
    }
}

private fun MutableList<QuestionPreviewHtmlEntity>.toQuestionData(): List<QuestionData> {
    return this.map {
        QuestionData(
            title = it.title,
            link = it.link,
            tags = it.tags,
        )
    }
}
