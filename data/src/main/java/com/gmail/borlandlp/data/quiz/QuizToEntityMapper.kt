package com.gmail.borlandlp.data.quiz

import com.gmail.borlandlp.data.quiz.local.QuizEntity
import com.gmail.borlandlp.domain.quiz.Quiz
import javax.inject.Inject

class QuizToEntityMapper @Inject constructor() {
    fun quizToEntity(quiz: Quiz): QuizEntity {
        return QuizEntity(
            id = quiz.id,
            done = quiz.done,
            tags = quiz.tags,
            questionsLinks = quiz.questionsLinks,
            createdUnixTime = quiz.createdUnixTime,
        )
    }

    fun entityToQuiz(entity: QuizEntity): Quiz {
        return Quiz(
            id = entity.id,
            done = entity.done,
            tags = entity.tags,
            questionsLinks = entity.questionsLinks,
            createdUnixTime = entity.createdUnixTime,
        )
    }
}