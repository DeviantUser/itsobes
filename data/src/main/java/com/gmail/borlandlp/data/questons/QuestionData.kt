package com.gmail.borlandlp.data.questons

class QuestionData (
    val title: String,
    val link: String,
    val tags: List<String>,
)