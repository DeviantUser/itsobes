package com.gmail.borlandlp.data.questons.local


import com.gmail.borlandlp.data.questons.QuestionData
import com.gmail.borlandlp.data.questons.QuestionSource
import com.gmail.borlandlp.domain.question.QuestionTag

class QuestionsCacheStorage(
    private val questionsDao: CacheQuestionsDao,
    private val remoteSource: QuestionSource,
) : QuestionSource {
    override suspend fun loadQuestionsByTag(tag: QuestionTag): List<QuestionData> {
        val cachedItems = questionsDao.getByTag(tag.id)
        if (cachedItems.isNotEmpty()) {
            return cachedItems.toQuestionData()
        }

        val questions = remoteSource.loadQuestionsByTag(tag)
        if (questions.isNotEmpty()) {
            questionsDao.insertAll(questions.toQuestionDbEntity())
        }

        return questions
    }

    override fun isCanHandleTag(tag: QuestionTag): Boolean {
        return remoteSource.isCanHandleTag(tag)
    }

    override fun isCanHandleUrl(url: String): Boolean {
        return remoteSource.isCanHandleUrl(url)
    }

    override fun loadQuestionByUrl(url: String): QuestionData? {
        val cachedItems = questionsDao.getQuestionFor(listOf(url)).firstOrNull()
        if (cachedItems != null) {
            return cachedItems.toQuestionData()
        }

        val question = remoteSource.loadQuestionByUrl(url)
        if (question != null) {
            questionsDao.insertAll(listOf(question.toQuestionDbEntity()))
        }

        return question
    }
}

private fun List<QuestionDbEntity>.toQuestionData(): List<QuestionData> {
    return this.map {
        it.toQuestionData()
    }
}

private fun QuestionDbEntity.toQuestionData(): QuestionData {
    return QuestionData(
        title = this.title,
        link = this.link,
        tags = this.tags,
    )
}

private fun List<QuestionData>.toQuestionDbEntity(): List<QuestionDbEntity> {
    return this.map {
        it.toQuestionDbEntity()
    }
}

private fun QuestionData.toQuestionDbEntity(): QuestionDbEntity {
    return QuestionDbEntity(
        title = this.title,
        link = this.link,
        tags = this.tags,
    )
}
