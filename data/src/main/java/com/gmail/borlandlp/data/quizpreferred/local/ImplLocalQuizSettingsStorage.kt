package com.gmail.borlandlp.data.quizpreferred.local

import android.content.SharedPreferences
import com.gmail.borlandlp.data.quizpreferred.SerializablePreferredSettings
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json

class ImplLocalQuizSettingsStorage (
    private val sharedPreferences: SharedPreferences,
) : LocalQuizSettingsStorage {
    override suspend fun get(): SerializablePreferredSettings? {
        return if (!sharedPreferences.contains(ENTITY_KEY)) {
            null
        } else {
            val jsonStr = sharedPreferences.getString(ENTITY_KEY, "")
            Json.decodeFromString(jsonStr!!)
        }
    }

    override suspend fun set(entity: SerializablePreferredSettings) {
        val encodedEntity = Json.encodeToString(entity)
        sharedPreferences.edit()
            .putString(ENTITY_KEY, encodedEntity)
            .apply()
    }


    companion object {
        private const val ENTITY_KEY = "shared_preferences_entity"
    }
}