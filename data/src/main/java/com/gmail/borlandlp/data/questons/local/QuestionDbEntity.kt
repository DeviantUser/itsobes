package com.gmail.borlandlp.data.questons.local

import androidx.room.ColumnInfo
import androidx.room.Entity

@Entity(primaryKeys = ["link"])
class QuestionDbEntity (
    @ColumnInfo(name = "title") var title: String,
    @ColumnInfo(name = "link") var link: String,
    @ColumnInfo(name = "tags") var tags: List<String>,
)