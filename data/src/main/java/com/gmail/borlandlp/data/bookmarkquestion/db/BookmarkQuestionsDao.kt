package com.gmail.borlandlp.data.bookmarkquestion.db

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query

@Dao
interface BookmarkQuestionsDao {
    @Query("SELECT * FROM BookmarkEntity WHERE link = :links")
    fun getBookmarkFor(links: String): BookmarkEntity?

    @Query("SELECT * FROM BookmarkEntity")
    fun getAllBookmarks(): List<BookmarkEntity>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(answers: List<BookmarkEntity>)

    @Delete
    fun remoteBookmarks(bookmarks: List<BookmarkEntity>)
}