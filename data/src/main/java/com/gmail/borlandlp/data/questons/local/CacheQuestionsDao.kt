package com.gmail.borlandlp.data.questons.local

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query

@Dao
interface CacheQuestionsDao {
    @Query("SELECT * FROM questiondbentity")
    fun getAll(): List<QuestionDbEntity>

    @Query("SELECT * FROM questiondbentity WHERE tags = :tag")
    fun getByTag(tag: String): List<QuestionDbEntity>

    @Query("SELECT * FROM questiondbentity WHERE tags IN (:tag)")
    fun getAllByTag(tag: List<String>): List<QuestionDbEntity>

    @Query("SELECT * FROM questiondbentity WHERE link IN (:links)")
    fun getQuestionFor(links: List<String>): List<QuestionDbEntity>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(questions: List<QuestionDbEntity>)
}