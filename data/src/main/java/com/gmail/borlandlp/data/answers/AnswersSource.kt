package com.gmail.borlandlp.data.answers

interface AnswersSource {
    suspend fun getByLink(link: String): AnswerData?
    fun isCanHandleLink(link: String): Boolean
}