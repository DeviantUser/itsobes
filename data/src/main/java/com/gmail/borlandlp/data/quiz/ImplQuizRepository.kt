package com.gmail.borlandlp.data.quiz

import com.gmail.borlandlp.data.quiz.local.LocalStorage
import com.gmail.borlandlp.domain.quiz.Quiz
import com.gmail.borlandlp.domain.quiz.QuizRepository
import javax.inject.Inject

class ImplQuizRepository @Inject constructor(
    private val localStorage: LocalStorage,
    private val mapper: QuizToEntityMapper,
) : QuizRepository {
    override suspend fun add(quiz: Quiz): Quiz {
        val result = localStorage.add(mapper.quizToEntity(quiz))
        return mapper.entityToQuiz(result)
    }

    override suspend fun remove(id: Int) {
        localStorage.remove(id)
    }

    override suspend fun save(quiz: Quiz) {
        localStorage.save(mapper.quizToEntity(quiz))
    }

    override suspend fun get(byIds: List<Int>): List<Quiz> {
        return localStorage.getByIds(byIds).map { mapper.entityToQuiz(it) }
    }

    override suspend fun getById(id: Int): Quiz {
        return localStorage.getByIds(listOf(id)).map { mapper.entityToQuiz(it) }.first()
    }
}