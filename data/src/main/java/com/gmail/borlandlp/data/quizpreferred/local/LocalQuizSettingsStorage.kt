package com.gmail.borlandlp.data.quizpreferred.local

import com.gmail.borlandlp.data.quizpreferred.SerializablePreferredSettings

interface LocalQuizSettingsStorage {
    suspend fun get(): SerializablePreferredSettings?
    suspend fun set(entity: SerializablePreferredSettings)
}