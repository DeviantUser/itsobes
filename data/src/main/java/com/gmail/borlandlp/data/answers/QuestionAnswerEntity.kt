package com.gmail.borlandlp.data.answers

import androidx.room.ColumnInfo
import androidx.room.Entity

@Entity(primaryKeys = ["link"])
class QuestionAnswerEntity (
    @ColumnInfo(name = "link") var link: String,
    @ColumnInfo(name = "content") var content: String,
    @ColumnInfo(name = "answer_tags") var tags: List<String>,
)