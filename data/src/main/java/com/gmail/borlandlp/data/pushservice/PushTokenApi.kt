package com.gmail.borlandlp.data.pushservice

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST

interface PushTokenApi {
    @POST("push/token")
    @FormUrlEncoded
    suspend fun setPushSettings(
        @Field("device_id") deviceId: String,
        @Field("push_token") token: String,
    ): SetPushTokenResponse
}

@Serializable
class SetPushTokenResponse(
    @SerialName("status_code")
    val statusCode: Int,
    @SerialName("error_message")
    val errorMessage: String? = null,
)