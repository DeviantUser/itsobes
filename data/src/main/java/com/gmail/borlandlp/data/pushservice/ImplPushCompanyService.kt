package com.gmail.borlandlp.data.pushservice

import com.gmail.borlandlp.data.DeviceContext
import com.gmail.borlandlp.domain.pushservice.AddPushCompanyResult
import com.gmail.borlandlp.domain.pushservice.DayOfWeek
import com.gmail.borlandlp.domain.pushservice.GetPushCompanySettingsResult
import com.gmail.borlandlp.domain.pushservice.PushCompany
import com.gmail.borlandlp.domain.pushservice.PushCompanyService
import com.gmail.borlandlp.domain.pushservice.RemovePushCompanySettingsResult
import com.gmail.borlandlp.domain.pushservice.SetPushCompanySettingsResult
import com.gmail.borlandlp.domain.pushservice.SwitchPushCompanyStateResult

class ImplPushCompanyService(
    private val remoteApi: PushCampaignApi,
    private val deviceContext: DeviceContext,
) : PushCompanyService {
    override suspend fun create(): AddPushCompanyResult {
        return remoteApi.add(deviceId = deviceContext.getDeviceId())
            .toResult()
    }

    override suspend fun put(company: PushCompany): SetPushCompanySettingsResult {
        return remoteApi.put(
            deviceId = deviceContext.getDeviceId(),
            campaignId = company.id,
            state = company.state,
            hours = company.hours,
            minutes = company.minutes,
            preferences = company.daysState
                .map { "preferences[${it.key.name}]" to it.value }
                .toMap(),
        ).toResult()
    }

    override suspend fun get(): GetPushCompanySettingsResult {
        return remoteApi.get(deviceId = deviceContext.getDeviceId())
            .toResult()
    }

    override suspend fun remove(pushCompanyId: Int): RemovePushCompanySettingsResult {
        return remoteApi.remove(
            deviceId = deviceContext.getDeviceId(),
            campaignId = pushCompanyId,
        ).toRemoveResult()
    }

    override suspend fun switchNotificationsState(newState: Boolean): SwitchPushCompanyStateResult {
        return remoteApi.updatePushNotificationSettings(
            deviceId = deviceContext.getDeviceId(),
            enabled = newState,
        ).toSwitchResult()
    }
}

private fun GetCompaniesResponse.toResult(): GetPushCompanySettingsResult {
    return if (this.statusCode == 200) {
        GetPushCompanySettingsResult.Success(
            data = this.data!!.settings.map { it.toPushCompany() },
            enabled = this.data.notificationsAreEnabled,
        )
    } else {
        GetPushCompanySettingsResult.Error(
            message = this.errorMessage,
        )
    }
}

private fun CompanyInfo.toPushCompany(): PushCompany {
    return PushCompany(
        id = this.campaignId,
        state = this.state,
        hours = this.hours,
        minutes = this.minutes,
        daysState = this.days.toDayState(),
    )
}

private fun List<DaySettings>.toDayState(): Map<DayOfWeek, Boolean> {
    val days = mutableMapOf<DayOfWeek, Boolean>()

    this.forEach {
         days[DayOfWeek.valueOf(it.id)] = it.state
    }

    return days
}

private fun CreateCompanyResponse.toResult(): AddPushCompanyResult {
    return if (this.statusCode == 200) {
        AddPushCompanyResult.Success(
            data = this.data!!.settings.toPushCompany(),
        )
    } else {
        AddPushCompanyResult.Error(
            message = this.errorMessage,
        )
    }
}

private fun StatusResponse.toResult(): SetPushCompanySettingsResult {
    return if (this.statusCode == 200) {
        SetPushCompanySettingsResult.Success
    } else {
        SetPushCompanySettingsResult.Error(this.errorMessage)
    }
}

private fun StatusResponse.toRemoveResult(): RemovePushCompanySettingsResult {
    return if (this.statusCode == 200) {
        RemovePushCompanySettingsResult.Success
    } else {
        RemovePushCompanySettingsResult.Error(this.errorMessage)
    }
}

private fun StatusResponse.toSwitchResult(): SwitchPushCompanyStateResult {
    return if (this.statusCode == 200) {
        SwitchPushCompanyStateResult.Success
    } else {
        SwitchPushCompanyStateResult.Error(this.errorMessage)
    }
}
