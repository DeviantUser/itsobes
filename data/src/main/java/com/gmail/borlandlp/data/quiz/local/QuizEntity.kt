package com.gmail.borlandlp.data.quiz.local

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "quiz")
data class QuizEntity (
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo("id") val id: Int,
    @ColumnInfo("done") val done: Boolean,
    @ColumnInfo("tags") val tags: List<String>,
    @ColumnInfo("questionsLinks") val questionsLinks: List<String>,
    @ColumnInfo("createdUnixTime") val createdUnixTime: Long,
)