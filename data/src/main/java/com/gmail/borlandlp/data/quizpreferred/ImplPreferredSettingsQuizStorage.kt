package com.gmail.borlandlp.data.quizpreferred

import com.gmail.borlandlp.data.quizpreferred.local.LocalQuizSettingsStorage
import com.gmail.borlandlp.domain.question.QuestionTag
import com.gmail.borlandlp.domain.quizpreferred.PreferredQuizSettingsStorage
import com.gmail.borlandlp.domain.quizpreferred.PreferredSettings

class ImplPreferredSettingsQuizStorage (
    private val localStorage: LocalQuizSettingsStorage,
) : PreferredQuizSettingsStorage {
    override suspend fun get(): PreferredSettings? {
        return localStorage.get()?.toPreferredSettings()
    }

    override suspend fun set(entity: PreferredSettings) {
        localStorage.set(entity.toEntity())
    }
}

private fun PreferredSettings.toEntity(): SerializablePreferredSettings {
    return SerializablePreferredSettings(
        questionsCount = this.questionsCount,
        tags = this.tags.map { it.id },
    )
}

private fun SerializablePreferredSettings.toPreferredSettings(): PreferredSettings {
    return PreferredSettings(
        questionsCount = this.questionsCount,
        tags = this.tags.map { QuestionTag.valueOf(it) },
    )
}