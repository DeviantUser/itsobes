package com.gmail.borlandlp.data.bookmarkquestion.db

import androidx.room.ColumnInfo
import androidx.room.Entity

@Entity(primaryKeys = ["link"])
class BookmarkEntity (
    @ColumnInfo(name = "link") var link: String,
    @ColumnInfo(name = "bookmarked_at") var unixtime: Long,
)