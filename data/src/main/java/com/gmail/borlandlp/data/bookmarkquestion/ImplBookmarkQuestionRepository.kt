package com.gmail.borlandlp.data.bookmarkquestion

import com.gmail.borlandlp.data.bookmarkquestion.db.BookmarkEntity
import com.gmail.borlandlp.data.bookmarkquestion.db.BookmarkQuestionsDao
import com.gmail.borlandlp.domain.bookmarkquestion.BookmarkQuestionRepository
import com.gmail.borlandlp.domain.bookmarkquestion.BookmarkedQuestion

class ImplBookmarkQuestionRepository(
    private val dao: BookmarkQuestionsDao,
) : BookmarkQuestionRepository {
    override suspend fun add(link: String) {
        dao.insertAll(listOf(
            BookmarkEntity(
            link = link,
            unixtime = System.currentTimeMillis() / 1000,
        )
        ))
    }

    override suspend fun get(link: String): BookmarkedQuestion? {
        return dao.getBookmarkFor(link).toBookmarkedQuestion()
    }

    override suspend fun getAll(): List<BookmarkedQuestion> {
        return dao.getAllBookmarks().toBookmarkedQuestions()
    }

    override suspend fun remove(bookmark: BookmarkedQuestion) {
        dao.remoteBookmarks(listOf(bookmark.toBookmarkEntity()))
    }
}

private fun BookmarkedQuestion.toBookmarkEntity(): BookmarkEntity {
    return BookmarkEntity(
        link = this.link,
        unixtime = this.markUnixTime,
    )
}

private fun List<BookmarkEntity>.toBookmarkedQuestions(): List<BookmarkedQuestion> {
    return this.map { it.toBookmarkedQuestion()!! }
}

private fun BookmarkEntity?.toBookmarkedQuestion(): BookmarkedQuestion? {
    return if (this == null) null else BookmarkedQuestion(
        link = this.link,
        markUnixTime = this.unixtime,
    )
}
