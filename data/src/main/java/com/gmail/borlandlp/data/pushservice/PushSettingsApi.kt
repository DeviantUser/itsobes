package com.gmail.borlandlp.data.pushservice

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import retrofit2.http.DELETE
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Path

interface PushSettingsApi {
    @POST("push/settings")
    @FormUrlEncoded
    suspend fun setPushSettings(
        @Field("device_id") deviceId: String,
        @Field("day_of_week") dayOfWeek: String,
        @Field("hours") hours: Int,
        @Field("minutes") minutes: Int,
        @Field("state") state: Boolean,
    ): SetPushSettingsResponse

    @GET("push/settings/{device_id}/{security_token}")
    suspend fun getPushSettings(
        @Path("device_id") deviceId: String,
    ): GetPushSettingsResponse

    @DELETE("push/settings/{device_id}/{security_token}/{day_of_week}")
    suspend fun deletePushSettings(
        @Path("device_id") deviceId: String,
        @Path("day_of_week") dayOfWeek: String,
    ): DeletePushSettingsResponse
}

@Serializable
class DeletePushSettingsResponse(
    @SerialName("status_code")
    val statusCode: Int,
    @SerialName("error_message")
    val errorMessage: String? = null,
)

@Serializable
class SetPushSettingsResponse(
    @SerialName("status_code")
    val statusCode: Int,
    @SerialName("error_message")
    val errorMessage: String? = null,
)

@Serializable
class GetPushSettingsResponse(
    @SerialName("status_code")
    val statusCode: Int,
    @SerialName("error_message")
    val errorMessage: String? = null,
    @SerialName("data")
    val data: List<Settings>,
)

@Serializable
class Settings(
    val hours: Int,
    val minutes: Int,
    val state: Boolean,
    @SerialName("day_of_week")
    val dayOfWeek: String,
)
