package com.gmail.borlandlp.data.dislikequiz.local

import javax.inject.Inject

class DislikeLocalStorage @Inject constructor(
    private val dao: DislikeQuestionDao,
) {
    suspend fun getDisliked(quizIds: List<String>): List<DislikeQuestionEntity> {
        return dao.getDisliked(quizIds)
    }

    suspend fun getAll(): List<DislikeQuestionEntity> {
        return dao.getAll()
    }

    suspend fun addDislike(quizId: String) {
        dao.insert(DislikeQuestionEntity(
            quizLink = quizId,
            state = true,
        ))
    }
    suspend fun removeDislike(quizIds: List<String>) {
        dao.delete(quizIds)
    }
}