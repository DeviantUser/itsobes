package com.gmail.borlandlp.data.answers

import com.gmail.borlandlp.domain.answer.Answer
import com.gmail.borlandlp.domain.answer.AnswersRepository
import javax.inject.Inject

class ImplAnswersRepository @Inject constructor(
    private val sources: List<AnswersSource>,
) : AnswersRepository {
    // answers - to refactor
    override suspend fun getBy(linkToSource: String): Answer? {
        val answer = sources.first { it.isCanHandleLink(linkToSource) }.getByLink(linkToSource) ?: return null

        return Answer(
            linkToSource = answer.link,
            content = answer.content,
            tags = answer.answerCustomTags,
        )
    }
}