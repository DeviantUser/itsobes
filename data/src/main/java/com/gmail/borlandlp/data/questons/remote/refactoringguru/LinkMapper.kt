package com.gmail.borlandlp.data.questons.remote.refactoringguru

import javax.inject.Inject

class LinkMapper @Inject constructor() {
    fun getCatalogueLink() = "https://refactoringguru.cn/ru/design-patterns/catalog"
    fun getBaseUrl() = "https://refactoringguru.cn"
}