package com.gmail.borlandlp.data.answers

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query

@Dao
interface CacheAnswersDao {
    @Query("SELECT * FROM QuestionAnswerEntity WHERE link = :links")
    fun getAnswerFor(links: String): QuestionAnswerEntity?

    @Query("SELECT * FROM QuestionAnswerEntity WHERE link IN (:links)")
    fun getAnswersFor(links: List<String>): List<QuestionAnswerEntity>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(answers: List<QuestionAnswerEntity>)
}