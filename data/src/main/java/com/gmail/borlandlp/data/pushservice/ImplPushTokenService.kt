package com.gmail.borlandlp.data.pushservice

import android.content.SharedPreferences
import com.gmail.borlandlp.data.DeviceContext
import com.gmail.borlandlp.domain.pushservice.GetTokenResult
import com.gmail.borlandlp.domain.pushservice.PushTokenService
import com.gmail.borlandlp.domain.pushservice.SetTokenResult
import com.google.firebase.messaging.FirebaseMessaging
import kotlinx.coroutines.tasks.await
import javax.inject.Inject

class ImplPushTokenService @Inject constructor(
    private val api: PushTokenApi,
    private val deviceContext: DeviceContext,
    private val sharedPreferences: SharedPreferences,
) : PushTokenService {
    override suspend fun set(pushToken: String): SetTokenResult {
        val result = try {
             api.setPushSettings(
                deviceId = deviceContext.getDeviceId(),
                token = pushToken,
            ).toResult()
        } catch (e: Exception) {
            SetTokenResult.NetworkException
        }

        if (result is SetTokenResult.Success) {
            sharedPreferences.edit()
                .putString("push_token", pushToken)
                .apply()
        }

        return result
    }

    override suspend fun get(): GetTokenResult {
        return GetTokenResult.Success(
            token = sharedPreferences.getString("push_token", "") ?: "",
        )
    }

    override suspend fun getInternalPushToken(): String {
        return FirebaseMessaging.getInstance().token.await()
    }
}

private fun SetPushTokenResponse.toResult(): SetTokenResult {
    return if (this.statusCode == 200) {
        SetTokenResult.Success
    } else {
        SetTokenResult.Error(
            message = this.errorMessage,
        )
    }
}
