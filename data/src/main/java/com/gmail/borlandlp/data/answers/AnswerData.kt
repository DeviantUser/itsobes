package com.gmail.borlandlp.data.answers

class AnswerData(
    val link: String,
    val answerCustomTags: List<String>,
    val content: String,
)