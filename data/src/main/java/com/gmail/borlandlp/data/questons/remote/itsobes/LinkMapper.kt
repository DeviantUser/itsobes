package com.gmail.borlandlp.data.questons.remote.itsobes

import com.gmail.borlandlp.domain.question.QuestionTag
import javax.inject.Inject

class LinkMapper @Inject constructor() {
    fun getLinkByType(tag: QuestionTag): String {
        return when (tag) {
            QuestionTag.JAVA -> "${getBaseUrl()}/JavaSobes/"
            QuestionTag.ANDROID -> "${getBaseUrl()}/AndroidSobes/"
            QuestionTag.IT -> "${getBaseUrl()}/ITSobes/"
            else -> throw RuntimeException("unknown tag type")
        }
    }

    fun getBaseUrl() = "https://itsobes.ru"
}