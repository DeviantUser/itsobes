package com.gmail.borlandlp.data.questons.remote

data class AnswerHtmlEntity (
    val title: String,
    val link: String,
    val tags: List<String>,
    val content: String,
)