package com.gmail.borlandlp.data.questons

import com.gmail.borlandlp.domain.question.Question
import com.gmail.borlandlp.domain.question.QuestionTag
import com.gmail.borlandlp.domain.question.QuestionsRepository

class ImplQuestionsRepository(
    private val sources: List<QuestionSource>,
) : QuestionsRepository {
    override suspend fun getAll(tags: List<QuestionTag>?): List<Question> {
        val requestedTags = tags ?: QuestionTag.entries.toList()
        val previewTags = mutableListOf<QuestionData>()

        sources.forEach { source ->
            requestedTags.forEach {
                if (source.isCanHandleTag(it)) {
                    previewTags.addAll(source.loadQuestionsByTag(it))
                }
            }
        }

        return previewTags.toQuestion()
    }

    override suspend fun get(linkUrl: String): Question? {
        var question: QuestionData? = null
        sources.forEach { source ->
            if (source.isCanHandleUrl(linkUrl)) {
                question = source.loadQuestionByUrl(linkUrl)
            }
        }
        if (question == null) {
            return null
        }

        return question!!.toQuestion()
    }
}

private fun List<QuestionData>.toQuestion(): List<Question> {
    return this.map { question ->
        Question(
            linkToSource = question.link,
            title = question.title,
            tags = question.tags.map {
                QuestionTag.valueOf(it)
            },
        )
    }
}

private fun QuestionData.toQuestion(): Question {
    return Question(
        linkToSource = this.link,
        title = this.title,
        tags = this.tags.map { QuestionTag.valueOf(it) },
    )
}
