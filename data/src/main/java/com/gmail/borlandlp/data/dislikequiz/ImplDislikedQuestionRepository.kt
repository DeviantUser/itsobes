package com.gmail.borlandlp.data.dislikequiz

import com.gmail.borlandlp.data.dislikequiz.local.DislikeLocalStorage
import com.gmail.borlandlp.domain.dislikequiz.DislikedQuestionRepository
import javax.inject.Inject

class ImplDislikedQuestionRepository @Inject constructor(
    private val localStorage: DislikeLocalStorage,
) : DislikedQuestionRepository {
    override suspend fun isDisliked(questionLink: String): Boolean {
        return localStorage.getDisliked(listOf(questionLink)).isNotEmpty()
    }

    override suspend fun getDisliked(questionLink: List<String>): List<String> {
        return localStorage.getDisliked(questionLink).map { it.quizLink }
    }

    override suspend fun getAll(): List<String> {
        return localStorage.getAll().map { it.quizLink }
    }

    override suspend fun addDislike(questionLink: String) {
        localStorage.addDislike(questionLink)
    }

    override suspend fun removeDislike(questionLink: String) {
        localStorage.removeDislike(listOf(questionLink))
    }
}
