package com.gmail.borlandlp.data.quiz.local

import javax.inject.Inject

class LocalStorage @Inject constructor(
    private val dao: QuizDao,
) {
    suspend fun add(quiz: QuizEntity): QuizEntity {
        val insertedItemId = dao.insert(quiz)
        return quiz.copy(id = insertedItemId.toInt())
    }

    suspend fun remove(id: Int) {
        dao.delete(id)
    }

    suspend fun save(quiz: QuizEntity) {
        dao.update(quiz)
    }

    suspend fun getByIds(ids: List<Int>): List<QuizEntity> {
        return dao.getByIds(ids)
    }
}