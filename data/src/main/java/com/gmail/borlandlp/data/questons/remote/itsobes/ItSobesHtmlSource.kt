package com.gmail.borlandlp.data.questons.remote.itsobes

import com.gmail.borlandlp.data.answers.AnswersSource
import com.gmail.borlandlp.data.questons.QuestionData
import com.gmail.borlandlp.data.questons.QuestionSource
import com.gmail.borlandlp.data.questons.remote.QuestionPreviewHtmlEntity
import com.gmail.borlandlp.domain.question.QuestionTag
import org.jsoup.Jsoup
import javax.inject.Inject

class ItSobesHtmlSource @Inject constructor (
    private val linkMapper: LinkMapper, // Вынести в отдельный класс?
): QuestionSource, AnswersSource {
    override suspend fun loadQuestionsByTag(tag: QuestionTag): List<QuestionData> {
        val link = linkMapper.getLinkByType(tag = tag)
        val doc = Jsoup.connect(link).get()
        val elements = doc.select(".content-column ul li a")
        val parsedElements = mutableListOf<QuestionPreviewHtmlEntity>()

        elements.iterator().forEach { element ->
            parsedElements.add(
                QuestionPreviewHtmlEntity(
                    title = element.text(),
                    link = linkMapper.getBaseUrl() + element.attr("href"),
                    tags = listOf(tag.id),
                )
            )
        }

        return parsedElements.toQuestionData()
    }

    override fun isCanHandleTag(tag: QuestionTag): Boolean {
        return SUPPORTED_TAGS.contains(tag)
    }

    override fun isCanHandleUrl(url: String): Boolean {
        return url.contains(linkMapper.getBaseUrl())
    }

    override fun loadQuestionByUrl(url: String): QuestionData? {
        TODO("Not yet implemented")
    }

    override suspend fun getByLink(link: String): com.gmail.borlandlp.data.answers.AnswerData? {
        val doc = Jsoup.connect(link).get()
        var content = doc.selectFirst("div[itemprop=acceptedAnswer] div")?.html()
        val tag = doc.selectFirst("li[class*=Header_active] a")?.text()

        content = content?.replace("/JavaSobes/", "https://itsobes.ru/JavaSobes/")
        content = content?.replace("/AndroidSobes/", "https://itsobes.ru/AndroidSobes/")
        content = content?.replace("/ITSobes/", "https://itsobes.ru/ITSobes/")

        return if (content.isNullOrEmpty()) {
            null
        } else {
            com.gmail.borlandlp.data.answers.AnswerData(
                link = link,
                answerCustomTags = listOf(tag!!),
                content = content,
            )
        }
    }

    override fun isCanHandleLink(link: String): Boolean {
        return link.contains("itsobes.ru")
    }

    companion object {
        private val SUPPORTED_TAGS = listOf(
            QuestionTag.ANDROID,
            QuestionTag.IT,
            QuestionTag.JAVA,
        )
    }
}

private fun MutableList<QuestionPreviewHtmlEntity>.toQuestionData(): List<QuestionData> {
    return this.map {
        QuestionData(
            title = it.title,
            link = it.link,
            tags = it.tags,
        )
    }
}
