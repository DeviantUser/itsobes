package com.gmail.borlandlp.data.quiz.local

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import androidx.room.Update

@Dao
interface QuizDao {
    @Query("SELECT * FROM quiz")
    fun getAll(): List<QuizEntity>

    @Query("SELECT * FROM quiz WHERE id IN (:id)")
    fun getByIds(id: List<Int>): List<QuizEntity>

    @Update(QuizEntity::class, OnConflictStrategy.REPLACE)
    fun update(entity: QuizEntity)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(entity: QuizEntity): Long

    @Query("DELETE FROM quiz WHERE id=:id")
    fun delete(id: Int)
}