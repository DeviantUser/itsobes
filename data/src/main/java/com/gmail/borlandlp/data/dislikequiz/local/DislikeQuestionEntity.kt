package com.gmail.borlandlp.data.dislikequiz.local

import androidx.room.ColumnInfo
import androidx.room.Entity

@Entity(primaryKeys = ["link"])
class DislikeQuestionEntity (
    @ColumnInfo(name = "link") val quizLink: String,
    @ColumnInfo(name = "state") val state: Boolean,
)