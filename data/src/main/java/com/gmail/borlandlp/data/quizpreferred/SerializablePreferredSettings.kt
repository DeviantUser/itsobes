package com.gmail.borlandlp.data.quizpreferred

import kotlinx.serialization.Serializable

@Serializable
class SerializablePreferredSettings(
    val questionsCount: Int,
    val tags: List<String>,
)