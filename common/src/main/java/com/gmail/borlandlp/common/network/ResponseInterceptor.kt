package com.gmail.borlandlp.common.network

import okhttp3.Interceptor
import okhttp3.Response
import okhttp3.ResponseBody
import okio.Buffer
import timber.log.Timber
import javax.inject.Inject

class ResponseInterceptor @Inject constructor() : Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {
        val originalRequest = chain.request()
        val response = chain.proceed(originalRequest)

        val responseBody = response.peekBody(Long.MAX_VALUE)
        val requestBodyBuffer = Buffer()
        originalRequest.body()?.writeTo(requestBodyBuffer)
        val responseBodyString = responseBody.string()
        Timber.d("response: %s", responseBodyString)

        val body = ResponseBody.create(responseBody.contentType(), responseBodyString)
        return response.newBuilder()
            .body(body)
            .build()
    }
}