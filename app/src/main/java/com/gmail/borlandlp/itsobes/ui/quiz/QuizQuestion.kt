package com.gmail.borlandlp.itsobes.ui.quiz

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.ripple.rememberRipple
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.constraintlayout.compose.ConstraintLayout
import androidx.constraintlayout.compose.Dimension
import com.gmail.borlandlp.itsobes.R
import com.gmail.borlandlp.itsobes.ui.theme.Black
import com.gmail.borlandlp.itsobes.ui.theme.SecondaryColor
import com.gmail.borlandlp.itsobes.ui.theme.White
import com.gmail.borlandlp.itsobes.utils.defaultBottomShadow

@Composable
@Preview(backgroundColor = 0xFFFFFFFF, showBackground = true, showSystemUi = true)
fun QuizQuestion(
    question: String = "Lorem ipsum dolores Lorem ipsum dolores Loreeeeee",
    onClick: (() -> Unit)? = null,
    isBookmarked: Boolean? = null,
    bookmarkClicked: (() -> Unit)? = null,
) {
    val cornerRadius = 24.dp
    val rootShape = RoundedCornerShape(cornerRadius)
    val constraintModifier = Modifier
    ConstraintLayout(
        modifier = constraintModifier
            .then(
                if (onClick != null) {
                    constraintModifier.clickable(
                        onClick = onClick,
                        interactionSource = remember { MutableInteractionSource() },
                        indication = rememberRipple(),
                    )
                } else {
                    constraintModifier
                }
            )
            .fillMaxWidth()
            .background(Black, rootShape)
            .padding(1.dp)
            .background(White, rootShape)
            .padding(
                start = 20.dp,
                top = 8.dp,
                end = 20.dp,
                bottom = 8.dp,
            ),
    ) {
        val (text, action) = createRefs()

        Text(
            text = question,
            modifier = Modifier
                .constrainAs(text) {
                    start.linkTo(parent.start)
                    top.linkTo(parent.top)
                    if (isBookmarked != null) {
                        end.linkTo(action.start)
                    } else {
                        end.linkTo(parent.end)
                    }
                    bottom.linkTo(parent.bottom)
                    width = Dimension.fillToConstraints
                },
        )

        if (isBookmarked != null) {
            Spacer(modifier = Modifier.width(16.dp))

            var state by remember {
                mutableStateOf(isBookmarked)
            }
            val icon = if (state) {
                painterResource(id = R.drawable.ic_bookmark_filled)
            } else {
                painterResource(id = R.drawable.ic_bookmark_stroke)
            }

            val imageSize = 21.dp
            val imageModifier = Modifier
            Image(
                painter = icon,
                contentDescription = if (state) {
                    "Remove bookmark status from question: $question"
                } else {
                    "Add bookmark status for question: $question"
                },
                modifier = imageModifier
                    .then(
                        if (bookmarkClicked != null) {
                            imageModifier.clickable(
                                onClick = {
                                    state = !state
                                    bookmarkClicked()
                                },
                                interactionSource = remember { MutableInteractionSource() },
                                indication = rememberRipple(radius = imageSize, bounded = false),
                            )
                        } else {
                            imageModifier
                        }
                    )
                    .padding(start = 8.dp)
                    .constrainAs(action) {
                        top.linkTo(parent.top)
                        end.linkTo(parent.end)
                    }
                    .defaultBottomShadow(CircleShape)
                    .clip(CircleShape)
                    .background(SecondaryColor)
                    .padding(11.dp)
                    .height(imageSize)
                    .width(imageSize),
            )
        }
    }
}


@Composable
@Preview(backgroundColor = 0xFFFFFFFF, showBackground = true, showSystemUi = true)
private fun QuizQuestionPreview0() {
    QuizQuestion(
        question = "aAAaaaAaaaaaaaAaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa",
        onClick = null,
        isBookmarked = null,
        bookmarkClicked = null,
    )
}

@Composable
@Preview(backgroundColor = 0xFFFFFFFF, showBackground = true, showSystemUi = true)
private fun QuizQuestionPreview1() {
    QuizQuestion(
        question = "Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum ",
        onClick = null,
        isBookmarked = null,
        bookmarkClicked = null,
    )
}

@Composable
@Preview(backgroundColor = 0xFFFFFFFF, showBackground = true, showSystemUi = true)
private fun QuizQuestionPreview2() {
    QuizQuestion(
        question = "aaaaaasasasasadsdsdsdsdsdsdsdsdsdsdsdssdsdsdsdsdssdsdsdsdasdasdasdasdadasdadadasdasdadasdadasdasd",
        onClick = { },
        isBookmarked = true,
        bookmarkClicked = {  },
    )
}
