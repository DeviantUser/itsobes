package com.gmail.borlandlp.itsobes.ui.choosequizparams

sealed interface Actions {
    class ShowError(val text: String) : Actions

    class MoveToQuiz(
        val quizId: Int,
    ) : Actions
}