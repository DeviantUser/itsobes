package com.gmail.borlandlp.itsobes.ui

import android.content.Intent
import android.net.Uri
import android.os.Build
import android.provider.Settings
import androidx.compose.material3.AlertDialog
import androidx.compose.material3.Button
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource

@Composable
fun OptionalRationalPermissionDialog (
    dismissCallback: () -> Unit
) {
    val context = LocalContext.current

    AlertDialog(
        onDismissRequest = { dismissCallback()},
        title = { Text(text = stringResource(com.gmail.borlandlp.itsobes.R.string.grant_permissions_title)) },
        text = { Text(text = stringResource(com.gmail.borlandlp.itsobes.R.string.grant_permissions_text)) },
        confirmButton = {
            Button(onClick = {
                val intent = Intent()
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    intent.setAction(Settings.ACTION_APP_NOTIFICATION_SETTINGS)
                    intent.putExtra(Settings.EXTRA_APP_PACKAGE, context.packageName)
                } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP){
                    intent.setAction("android.settings.APP_NOTIFICATION_SETTINGS")
                    intent.putExtra("app_package", context.packageName)
                    intent.putExtra("app_uid", context.applicationInfo.uid)
                } else {
                    intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
                    intent.addCategory(Intent.CATEGORY_DEFAULT)
                    intent.setData(Uri.parse("package:" + context.packageName))
                }
                context.startActivity(intent)
            }) {
                Text(text = stringResource(com.gmail.borlandlp.itsobes.R.string.move_to_settings))
            }
        },
        dismissButton = {
            Button(onClick = {
                dismissCallback()
            }) {
                Text(text = stringResource(com.gmail.borlandlp.itsobes.R.string.cancel))
            }
        },
    )
}