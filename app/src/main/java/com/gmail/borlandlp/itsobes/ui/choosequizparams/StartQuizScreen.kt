package com.gmail.borlandlp.itsobes.ui.choosequizparams

import android.widget.Toast
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.hilt.navigation.compose.hiltViewModel
import com.gmail.borlandlp.itsobes.R
import com.gmail.borlandlp.itsobes.ui.FilledButtonUi
import com.gmail.borlandlp.itsobes.ui.NoConnectionViewCompose
import com.gmail.borlandlp.itsobes.ui.ProgressComposeView
import com.gmail.borlandlp.itsobes.ui.actionbar.SimpleActionBarUi
import com.gmail.borlandlp.itsobes.ui.theme.TypographyPrimary
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

@Composable
fun StartQuizScreen(
    viewModel: ChooseQuizParamsViewModel = hiltViewModel(),
    navigateToQuiz: (quizId: Int) -> Unit,
) {
    val scope = rememberCoroutineScope()
    val context = LocalContext.current
    LaunchedEffect(true) {
        scope.launch {
            viewModel.actions.collect {
                when (it) {
                    is Actions.MoveToQuiz -> {
                        withContext(Dispatchers.Main) {
                            navigateToQuiz(it.quizId)
                        }
                    }
                    is Actions.ShowError -> {
                        Toast.makeText(
                            context,
                            it.text,
                            Toast.LENGTH_LONG,
                        ).show()
                    }
                    else -> {}
                }
            }
        }
    }

    if (viewModel.uiState.isLoading) {
        ProgressComposeView(
            text = stringResource(R.string.load_data),
        )
    } else if (viewModel.uiState.networkError) {
        NoConnectionViewCompose(
            onBtnReloadCLicked = { viewModel.loadPreferSettings() },
        )
    } else {
        Scaffold(
            topBar = {
                SimpleActionBarUi(
                    title = stringResource(R.string.quiz),
                )
            },
        ) {
            Column(
                horizontalAlignment = Alignment.CenterHorizontally,
                modifier = Modifier
                    .fillMaxSize()
                    .background(Color.White)
                    .padding(16.dp)
                    .padding(paddingValues = it),
            ) {
                Text(
                    text = stringResource(R.string.choose_quiz_tags),
                    modifier = Modifier.padding(top = 8.dp),
                    fontSize = 16.sp,
                    color = TypographyPrimary,
                )

                TagsGroup(tagUiState = viewModel.uiState.settingsState.tags) { newTagState ->
                    viewModel.onFilterStateChanged(newTagState)
                }

                Spacer(
                    modifier = Modifier
                        .fillMaxWidth()
                        .height(24.dp),
                )

                Text(
                    text = stringResource(R.string.set_questions_amount),
                    color = TypographyPrimary,
                )

                Spacer(
                    modifier = Modifier
                        .fillMaxWidth()
                        .height(10.dp),
                )

                BorderedTextField(
                    value = viewModel.uiState.settingsState.questionsCount.toString(),
                    onValueChange = { newAmount -> viewModel.onSetQuestionsLimit(newAmount) },
                )

                Spacer(
                    modifier = Modifier
                        .fillMaxWidth()
                        .height(24.dp),
                )

                FilledButtonUi(
                    text = "Начать квиз",
                    onClick = { viewModel.onStartQuizClicked() },
                )
            }
        }
    }
}