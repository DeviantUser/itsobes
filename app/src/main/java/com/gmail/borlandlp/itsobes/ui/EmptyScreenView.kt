package com.gmail.borlandlp.itsobes.ui

import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.constraintlayout.compose.ConstraintLayout
import com.airbnb.lottie.compose.LottieAnimation
import com.airbnb.lottie.compose.LottieCompositionSpec
import com.airbnb.lottie.compose.rememberLottieComposition
import com.gmail.borlandlp.itsobes.R
import com.gmail.borlandlp.itsobes.ui.theme.Black

@Composable
@Preview
fun EmptyDataScreenUi() {
    ConstraintLayout(modifier = Modifier.fillMaxSize()) {
        val (image, text) = createRefs()
        val composition by rememberLottieComposition(LottieCompositionSpec.RawRes(R.raw.animation_empty_data))
        LottieAnimation(
            composition = composition,
            iterations = Int.MAX_VALUE,
            modifier = Modifier
                .height(200.dp)
                .width(200.dp)
                .constrainAs(image) {
                    top.linkTo(parent.top)
                    bottom.linkTo(parent.bottom)
                    start.linkTo(parent.start)
                    end.linkTo(parent.end)
                },
        )

        Text(
            text = stringResource(id = R.string.empty_screen_favorites),
            color = Black,
            fontSize = 16.sp,
            modifier = Modifier.padding(top = 2.dp)
                .constrainAs(text) {
                    top.linkTo(image.bottom)
                    start.linkTo(parent.start)
                    end.linkTo(parent.end)
                },
        )
    }
}