package com.gmail.borlandlp.itsobes.ui.settings

import android.os.Parcel
import android.os.Parcelable

data class PushUiState(
    var enabled: Boolean,
    var time: TimeUiState?,
    var dayOfWeekUiState: DayOfWeekUiState,
) : Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readByte() != 0.toByte(),
        parcel.readParcelable(TimeUiState::class.java.classLoader),
        parcel.readParcelable(DayOfWeekUiState::class.java.classLoader)!!,
    ) {}

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeByte(if (enabled) 1 else 0)
        parcel.writeParcelable(time, flags)
        parcel.writeParcelable(dayOfWeekUiState, flags)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<PushUiState> {
        override fun createFromParcel(parcel: Parcel): PushUiState {
            return PushUiState(parcel)
        }

        override fun newArray(size: Int): Array<PushUiState?> {
            return arrayOfNulls(size)
        }
    }
}
