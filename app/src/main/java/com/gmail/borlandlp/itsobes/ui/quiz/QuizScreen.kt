package com.gmail.borlandlp.itsobes.ui.quiz

import android.widget.Toast
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Scaffold
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalUriHandler
import androidx.compose.ui.tooling.preview.Preview
import androidx.hilt.navigation.compose.hiltViewModel
import com.gmail.borlandlp.itsobes.R
import com.gmail.borlandlp.itsobes.ui.NoConnectionViewCompose
import com.gmail.borlandlp.itsobes.ui.ProgressComposeView
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext


@Composable
@Preview
fun QuizScreen(
    viewModel: QuizViewModel = hiltViewModel(),
    navigateToNextPageQuiz: (quizId: Int, page: Int) -> Unit = { _, _ -> },
    navigateUp: () -> Unit = {},
    navigateToMain: () -> Unit = {},
) {
    val scope = rememberCoroutineScope()
    LaunchedEffect(true) {
        scope.launch {
            viewModel.navigationEvent.collect {
                when (it) {
                    is NavigationEvent.MoveToNextQuiz -> {
                        withContext(Dispatchers.Main) {
                            navigateToNextPageQuiz(it.quizId, it.nextQuestionNum)
                        }
                    }
                    is NavigationEvent.MoveToMain -> {
                        withContext(Dispatchers.Main) {
                            navigateToMain()
                        }
                    }
                }
            }
        }
    }

    if (viewModel.uiState.isLoading) {
        ProgressComposeView(
            text = "",
        )
    } else if (viewModel.uiState.networkError) {
        NoConnectionViewCompose(
            onBtnReloadCLicked = { viewModel.onNeedLoadQuestion() },
        )
    } else {
        val uriHandler = LocalUriHandler.current
        val context = LocalContext.current
        Scaffold(
            topBar = {
                QuizTopAppBar(
                    currentQuestion = viewModel.uiState.questionState.currentQuestionIndex + 1,
                    totalQuestions = viewModel.uiState.questionState.totalQuestions,
                    navigateUp = navigateUp,
                    isBlacklisted = viewModel.uiState.isDisliked,
                    blacklistClicked = {
                        if (viewModel.uiState.isDisliked) {
                            Toast.makeText(
                                context,
                                context.getString(R.string.undo_blocklist_for_question),
                                Toast.LENGTH_SHORT
                            ).show()
                        } else {
                            Toast.makeText(
                                context,
                                context.getString(R.string.question_blacklisted),
                                Toast.LENGTH_SHORT
                            ).show()
                        }
                        viewModel.switchLikeQuestionState()
                    },
                )
            }
        ) { paddingValues ->
            Column(modifier = Modifier
                .fillMaxSize()
                .padding(paddingValues = paddingValues)) {
                QuizUI(
                    tags = viewModel.uiState.questionState.tags,
                    isShowingAnswer = viewModel.uiState.isShowingAnswer,
                    question = viewModel.uiState.questionState.question,
                    answer = viewModel.uiState.questionState.answer,
                    questionLink = viewModel.uiState.questionState.linkToSource,
                    onLinkClicked = { uriHandler.openUri(it) },
                    onShowAnswerClicked = { viewModel.showAnswer() },
                    onNextQuestionClicked = {
                        if (viewModel.uiState.questionState.isLast) {
                            viewModel.finishQuizBtnClicked()
                        } else {
                            viewModel.nextQuestionBtnClicked()
                        }
                    },
                    onBookmarkClicked = { viewModel.switchBookmarkStatus() },
                    isLastQuestion = viewModel.uiState.questionState.isLast,
                    isBookmarked = viewModel.uiState.isBookmarked,
                )
            }
        }
    }
}