package com.gmail.borlandlp.itsobes.ui.settings

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material.ripple.rememberRipple
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.painter.Painter
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import com.gmail.borlandlp.itsobes.ui.theme.SecondaryColor
import com.gmail.borlandlp.itsobes.ui.theme.TypographyPrimary
import com.gmail.borlandlp.itsobes.utils.defaultBottomShadow

@Composable
fun LongFavoriteBtnView(
    onBookmarkClicked: () -> Unit,
    basePadding: Dp,
    imageSize: Dp,
    bookmarkImage: Painter,
    favoriteText: String,
) {
    Row(
        modifier = Modifier
            .defaultBottomShadow(CircleShape)
            .clickable(
                onClick = {
                    onBookmarkClicked()
                },
                interactionSource = remember { MutableInteractionSource() },
                indication = rememberRipple(),
            )
            .clip(CircleShape)
            .background(SecondaryColor)
            .padding(start = 10.dp, end = 10.dp)
            .height(imageSize + basePadding + 6.dp),
        verticalAlignment = Alignment.CenterVertically,
    ) {
        Spacer(modifier = Modifier.width(basePadding))

        Image(
            painter = bookmarkImage,
            contentDescription = favoriteText,
            modifier = Modifier
                .clickable { onBookmarkClicked() }
                .height(imageSize)
                .width(imageSize),
        )

        Spacer(modifier = Modifier.width(basePadding))

        Text(
            text = favoriteText,
            color = TypographyPrimary,
        )

        Spacer(modifier = Modifier.width(basePadding))
    }
}