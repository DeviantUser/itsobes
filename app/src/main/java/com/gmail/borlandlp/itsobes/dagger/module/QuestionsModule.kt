package com.gmail.borlandlp.itsobes.dagger.module

import com.gmail.borlandlp.data.questons.ImplQuestionsRepository
import com.gmail.borlandlp.data.questons.local.CacheQuestionsDao
import com.gmail.borlandlp.data.questons.local.QuizDatabase
import com.gmail.borlandlp.data.questons.local.QuestionsCacheStorage
import com.gmail.borlandlp.data.questons.remote.itsobes.ItSobesHtmlSource
import com.gmail.borlandlp.data.questons.remote.refactoringguru.GuruSource
import com.gmail.borlandlp.domain.question.QuestionsRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ViewModelComponent

@Module(includes = [QuizModule::class])
@InstallIn(ViewModelComponent::class)
class QuestionsModule {
    @Provides
    fun provideQuestionSource(
        questionsDao: CacheQuestionsDao,
        htmlSource: ItSobesHtmlSource,
        guruSource: GuruSource,
    ): QuestionsRepository {
        return ImplQuestionsRepository(
            sources = listOf(
                QuestionsCacheStorage(questionsDao, htmlSource),
                QuestionsCacheStorage(questionsDao, guruSource),
            ),
        )
    }

    @Provides
    fun getCacheQuestionsDao(db: QuizDatabase): CacheQuestionsDao {
        return db.cacheQuestionsDao()
    }
}