package com.gmail.borlandlp.itsobes.ui.bookmarkedquestion

import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.gmail.borlandlp.domain.bookmarkquestion.BookmarkQuestionFullInfo
import com.gmail.borlandlp.domain.bookmarkquestion.GetBookmarkedQuestionInteractor
import com.gmail.borlandlp.domain.bookmarkquestion.SwitchBookmarkResult
import com.gmail.borlandlp.domain.bookmarkquestion.SwitchBookmarkStatusInteractor
import com.gmail.borlandlp.itsobes.ui.navigation.AppDestinations
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.net.URLDecoder
import java.nio.charset.StandardCharsets
import javax.inject.Inject

@HiltViewModel
class BookmarkedQuestionViewModel @Inject constructor(
    private val getBookmarkedQuestionInteractor: GetBookmarkedQuestionInteractor,
    private val switchBookmarkStatusInteractor: SwitchBookmarkStatusInteractor,
    private val savedStateHandle: SavedStateHandle,
) : ViewModel() {
    private fun getQuestionUrl(): String {
        val encodedUrl = savedStateHandle.get<String>(AppDestinations.FAVORITE_QUESTION_PARAM_QUESTION_URL)!!
        return URLDecoder.decode(encodedUrl, StandardCharsets.UTF_8.toString()) // TODO Вынести отсюда эту логику
    }
    var uiState by mutableStateOf(BookmarkedQuestionsUiState())
        private set

    init {
        loadBookmark()
    }

    fun loadBookmark() {
        uiState = uiState.copy(dataIsLoading = true)
        viewModelScope.launch {
            withContext(Dispatchers.IO) {
                val question = getBookmarkedQuestionInteractor.execute(getQuestionUrl()).toUiState()
                withContext(Dispatchers.Main) {
                    uiState = uiState.copy(
                        dataIsLoading = false,
                        state = question,
                    )
                }
            }
        }
    }

    fun switchBookmarkStatus() {
        viewModelScope.launch {
            val result = switchBookmarkStatusInteractor.execute(getQuestionUrl())
            withContext(Dispatchers.Main) {
                uiState = when(result) {
                    is SwitchBookmarkResult.AddedToBookmark, is SwitchBookmarkResult.BookmarkIsRemoved -> {
                        uiState.copy(
                            isBookmarked = result is SwitchBookmarkResult.AddedToBookmark,
                        )
                    }

                    is SwitchBookmarkResult.Error -> {
                        uiState.copy(
                            isBookmarked = !uiState.isBookmarked,
                        )
                    }
                }
            }
        }
    }
}

private fun BookmarkQuestionFullInfo.toUiState(): BookmarkUiState {
    return BookmarkUiState(
        title = this.title,
        tags = this.tags,
        content = this.content,
        link = this.link,
    )
}
