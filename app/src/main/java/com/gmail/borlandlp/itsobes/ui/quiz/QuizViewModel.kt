package com.gmail.borlandlp.itsobes.ui.quiz

import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.gmail.borlandlp.domain.bookmarkquestion.SwitchBookmarkResult
import com.gmail.borlandlp.domain.bookmarkquestion.SwitchBookmarkStatusInteractor
import com.gmail.borlandlp.domain.dislikequiz.SwitchLikeStatusForQuestionInteractor
import com.gmail.borlandlp.domain.dislikequiz.SwitchResult
import com.gmail.borlandlp.domain.quiz.GetAnswerInteractor
import com.gmail.borlandlp.itsobes.ui.navigation.AppDestinations
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.asSharedFlow
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject

@HiltViewModel
class QuizViewModel @Inject constructor (
    private val savedStateHandle: SavedStateHandle,
    private val getQuestionInteractor: GetAnswerInteractor,
    private val switchLikeInteractor: SwitchLikeStatusForQuestionInteractor,
    private val switchBookmarkStatusInteractor: SwitchBookmarkStatusInteractor,
)  : ViewModel() {
    private fun getQuestionLink(): String = uiState.questionState.linkToSource
    private fun getQuizId(): Int = savedStateHandle[AppDestinations.QUIZ_PARAM_ID]!!
    private fun getQuestionPos(): Int = savedStateHandle[AppDestinations.QUIZ_PARAM_QUESTION_NUM]!!

    var uiState by mutableStateOf(QuizPageState())
        private set
    private val _navigationEvent = MutableSharedFlow<NavigationEvent>()
    val navigationEvent = _navigationEvent.asSharedFlow()

    init {
        onNeedLoadQuestion()
    }

    fun onNeedLoadQuestion() {
        uiState = uiState.copy(isLoading = true)
        viewModelScope.launch {
            withContext(Dispatchers.IO) {
                val state = getQuestionInteractor.execute(
                    quizId = getQuizId(),
                    questionPos = getQuestionPos(),
                )

                withContext(Dispatchers.Main) {
                    uiState = uiState.copy(
                        isLoading = false,
                        questionState = state,
                    )
                }
            }
        }
    }

    fun showAnswer() {
        uiState = uiState.copy(isShowingAnswer = true)
    }

    fun switchBookmarkStatus() {
        viewModelScope.launch {
            withContext(Dispatchers.IO) {
                val result = switchBookmarkStatusInteractor.execute(getQuestionLink())
                withContext(Dispatchers.Main) {
                    uiState = when(result) {
                        is SwitchBookmarkResult.AddedToBookmark, is SwitchBookmarkResult.BookmarkIsRemoved -> {
                            uiState.copy(isBookmarked = result is SwitchBookmarkResult.AddedToBookmark)
                        }

                        is SwitchBookmarkResult.Error -> {
                            uiState.copy(isBookmarked = !uiState.isBookmarked)
                        }
                    }
                }
            }
        }
    }

    fun switchLikeQuestionState() {
        viewModelScope.launch {
            withContext(Dispatchers.IO) {
                val result = switchLikeInteractor.execute(getQuestionLink())
                withContext(Dispatchers.Main) {
                    uiState = when(result) {
                        is SwitchResult.Liked, is SwitchResult.Disliked -> {
                            uiState.copy(isDisliked = result is SwitchResult.Disliked)
                        }
                        is SwitchResult.Error -> {
                            uiState.copy(isDisliked = !uiState.isDisliked)
                        }
                    }
                }
            }
        }
    }

    fun finishQuizBtnClicked() {
        viewModelScope.launch {
            withContext(Dispatchers.Main) {
                _navigationEvent.emit(NavigationEvent.MoveToMain())
            }
        }
    }

    fun nextQuestionBtnClicked() {
        viewModelScope.launch {
            withContext(Dispatchers.IO) {
                withContext(Dispatchers.Main) {
                    _navigationEvent.emit(NavigationEvent.MoveToNextQuiz(
                        nextQuestionNum = getQuestionPos() + 1,
                        quizId = getQuizId(),
                    ))
                }
            }
        }
    }
}
