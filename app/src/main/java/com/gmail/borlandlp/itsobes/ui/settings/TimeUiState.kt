package com.gmail.borlandlp.itsobes.ui.settings

import android.os.Parcel
import android.os.Parcelable
import com.gmail.borlandlp.domain.pushservice.DayOfWeek

data class TimeUiState(
    val hours: Int,
    val minutes: Int,
) : Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readInt(),
        parcel.readInt()
    ) {}

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(hours)
        parcel.writeInt(minutes)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<TimeUiState> {
        override fun createFromParcel(parcel: Parcel): TimeUiState {
            return TimeUiState(parcel)
        }

        override fun newArray(size: Int): Array<TimeUiState?> {
            return arrayOfNulls(size)
        }
    }

}

class DayOfWeekUiState(
    val id: DayOfWeek,
) : Parcelable {
    constructor(parcel: Parcel) : this(
        DayOfWeek.valueOf(parcel.readString()!!),
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(id.name)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<DayOfWeekUiState> {
        override fun createFromParcel(parcel: Parcel): DayOfWeekUiState {
            return DayOfWeekUiState(parcel)
        }

        override fun newArray(size: Int): Array<DayOfWeekUiState?> {
            return arrayOfNulls(size)
        }
    }
}

fun DayOfWeekUiState.getTitleOfDay(): String {
    return when(this.id) {
        DayOfWeek.Monday -> "Понедельник"
        DayOfWeek.Tuesday -> "Вторник"
        DayOfWeek.Wednesday -> "Среда"
        DayOfWeek.Thursday -> "Четверг"
        DayOfWeek.Friday -> "Пятница"
        DayOfWeek.Saturday -> "Суббота"
        DayOfWeek.Sunday -> "Воскресенье"
    }
}
