package com.gmail.borlandlp.itsobes.dagger.module.core

import android.content.Context
import com.gmail.borlandlp.common.network.ResponseInterceptor
import com.jakewharton.retrofit2.converter.kotlinx.serialization.asConverterFactory
import dagger.Module
import dagger.Provides
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.migration.DisableInstallInCheck
import kotlinx.serialization.json.Json
import okhttp3.Cache
import okhttp3.MediaType
import okhttp3.OkHttpClient
import retrofit2.Converter
import java.io.File
import java.util.concurrent.TimeUnit

@Module
@DisableInstallInCheck
class OkHttpModule {
    @Provides
    fun okHttpClient(
        cache: Cache,
        interceptor: ResponseInterceptor,
    ): OkHttpClient {
        return OkHttpClient()
            .newBuilder()
            .cache(cache)
            .connectTimeout(60, TimeUnit.SECONDS)
            .writeTimeout(60, TimeUnit.SECONDS)
            .readTimeout(60, TimeUnit.SECONDS)
            .addInterceptor(interceptor)
            .build()
    }

    @Provides
    fun cache(cacheFile: File): Cache {
        return Cache(cacheFile, 10 * 1000 * 1000) //10 MB
    }

    @Provides
    fun file(
        @ApplicationContext context: Context
    ): File {
        val file = File(context.cacheDir, "HttpCache")
        file.mkdirs()
        return file
    }

    @Provides
    fun kotlinConverterFactory(): Converter.Factory {
        val contentType = MediaType.get("application/json")
        val json = Json(from = Json.Default) {
            ignoreUnknownKeys = true
        }

        return json.asConverterFactory(contentType)
    }
}
