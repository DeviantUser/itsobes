package com.gmail.borlandlp.itsobes.dagger.module

import com.gmail.borlandlp.domain.bookmarkquestion.BookmarkQuestionRepository
import com.gmail.borlandlp.data.bookmarkquestion.ImplBookmarkQuestionRepository
import com.gmail.borlandlp.data.bookmarkquestion.db.BookmarkQuestionsDao
import com.gmail.borlandlp.data.questons.local.QuizDatabase
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ViewModelComponent

@Module(includes = [QuizDbModule::class])
@InstallIn(ViewModelComponent::class)
class BookmarkQuestionModule {
    @Provides
    fun getRepository(
        dao: BookmarkQuestionsDao,
    ): BookmarkQuestionRepository {
        return ImplBookmarkQuestionRepository(
            dao = dao,
        )
    }

    @Provides
    fun provideDao(db: QuizDatabase): BookmarkQuestionsDao {
        return db.getBookmarkQuestionsDao()
    }
}
