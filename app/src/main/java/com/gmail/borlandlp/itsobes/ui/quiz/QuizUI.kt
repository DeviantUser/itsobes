package com.gmail.borlandlp.itsobes.ui.quiz

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.ExperimentalLayoutApi
import androidx.compose.foundation.layout.FlowRow
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.verticalScroll
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.gmail.borlandlp.itsobes.R
import com.gmail.borlandlp.itsobes.ui.FilledButtonUi
import com.gmail.borlandlp.itsobes.ui.TransparentButtonUi
import com.gmail.borlandlp.itsobes.ui.settings.LongFavoriteBtnView
import com.gmail.borlandlp.itsobes.ui.theme.SecondaryColor
import com.gmail.borlandlp.itsobes.utils.defaultBottomShadow

@OptIn(ExperimentalLayoutApi::class)
@Composable
fun QuizUI(
    tags: List<String> = emptyList(),
    isShowingAnswer: Boolean = false,
    question: String = "",
    answer: String = "",
    questionLink: String = "",
    isLastQuestion: Boolean = false,
    onLinkClicked: (link: String) -> Unit = {},
    onShowAnswerClicked: () -> Unit = {},
    onNextQuestionClicked: () -> Unit = {},
    onBookmarkClicked: () -> Unit = {},
    isBookmarked: Boolean = false,
) {
    val scrollState = rememberScrollState()
    val containerModifiers = Modifier
        .fillMaxSize()
        .background(Color.White)
        .verticalScroll(scrollState)
        .padding(16.dp)

    Column(
        modifier = containerModifiers,
    ) {
        QuestionTagsListUi(
            tags = tags,
        )

        Spacer(
            modifier = Modifier
                .fillMaxWidth()
                .height(14.dp),
        )

        QuizQuestion(
            question = question,
        )

        SourceQuestionUi(
            link = questionLink,
            onLinkClicked = onLinkClicked,
        )

        if (isShowingAnswer) {
            AnswerTextUi(answer = answer)
        }

        FlowRow(
            horizontalArrangement = Arrangement.spacedBy(space = 16.dp, alignment = Alignment.CenterHorizontally),
            verticalArrangement = Arrangement.spacedBy(space = 16.dp, alignment = Alignment.Top),
            modifier = Modifier.fillMaxWidth()
                .padding(top = 16.dp)
        ) {
            if (!isShowingAnswer) {
                FilledButtonUi(
                    text = stringResource(id = R.string.show_answer),
                    onClick = onShowAnswerClicked,
                )
            }

            val bookmarkImage = if (isBookmarked) {
                painterResource(id = R.drawable.ic_bookmark_filled)
            } else {
                painterResource(id = R.drawable.ic_bookmark_stroke)
            }
            val imageSize = 25.dp
            val basePadding = 10.dp
            val favoriteText = if (!isBookmarked) {
                stringResource(R.string.add_to_favorites)
            } else {
                stringResource(R.string.remove_from_favorites)
            }
            if (!isShowingAnswer) {
                Image(
                    painter = bookmarkImage,
                    contentDescription = favoriteText,
                    modifier = Modifier
                        .defaultBottomShadow(CircleShape)
                        .clickable { onBookmarkClicked() }
                        .clip(CircleShape)
                        .background(SecondaryColor)
                        .padding(basePadding)
                        .height(imageSize)
                        .width(imageSize),
                )
            } else {
                LongFavoriteBtnView(
                    onBookmarkClicked = onBookmarkClicked,
                    basePadding = basePadding,
                    imageSize = imageSize,
                    bookmarkImage = bookmarkImage,
                    favoriteText = favoriteText,
                )
            }

            TransparentButtonUi(
                text = stringResource(id = if (isLastQuestion) R.string.finish else R.string.next),
                onClick = onNextQuestionClicked,
            )
        }
    }
}

@Composable
@Preview
private fun TestQuizUI() {
    QuizUI(
        tags = listOf("Android", "Java", "Kotlin", "RxJava", "Some", "Hello world!"),
        isShowingAnswer = false,
        question = "Какой лучший язык программирования и это именно kotlin?",
        questionLink = "https://habr.com/ru/articles/783456/",
        isLastQuestion = true,
        isBookmarked = true,
        answer = stringResource(R.string.lorem_ipsum),
    )
}