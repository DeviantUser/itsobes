package com.gmail.borlandlp.itsobes.ui.choosequizparams

import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.gmail.borlandlp.domain.question.QuestionTag
import com.gmail.borlandlp.domain.quiz.RunQuizInteractor
import com.gmail.borlandlp.domain.quizpreferred.GetPreferQuizSettingsInteractor
import com.gmail.borlandlp.domain.quizpreferred.QuizPreferredTag
import com.gmail.borlandlp.domain.quizpreferred.SetPreferQuizSettingsInteractor
import com.gmail.borlandlp.domain.quizpreferred.UserPreferSettings
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.SharedFlow
import kotlinx.coroutines.flow.asSharedFlow
import kotlinx.coroutines.launch
import kotlinx.coroutines.plus
import kotlinx.coroutines.withContext
import javax.inject.Inject

@HiltViewModel
class ChooseQuizParamsViewModel @Inject constructor (
    private val savedStateHandle: SavedStateHandle, // TODO Здесь устаревшая логика с ним???
    private val randomQuestionsInteractor: RunQuizInteractor,
    private val preferQuizSettingsInteractor: GetPreferQuizSettingsInteractor,
    private val setPreferQuizSettingsInteractor: SetPreferQuizSettingsInteractor,
    private val errorCoroutineExceptionHandler: CoroutineExceptionHandler,
) : ViewModel() {
    private val _actions = MutableSharedFlow<Actions>()
    val actions: SharedFlow<Actions> = _actions.asSharedFlow()
    var uiState by mutableStateOf(ChooseQuizParamsUiState())
        private set

    init {
        loadPreferSettings()
    }

    fun onStartQuizClicked() {
        uiState = uiState.copy(isLoading = true)

        (viewModelScope + SupervisorJob() + errorCoroutineExceptionHandler + getExceptionHandler()).launch {
            withContext(Dispatchers.IO) {
                val settingsUiState = savedStateHandle.get<ChooseQuizParamsUiState>(PREFER_STORAGE_KEY)

                // save current quiz settings
                val settings = settingsUiState!!.settingsState.toPreferSettings()
                setPreferQuizSettingsInteractor.execute(settings)

                // create a new quiz
                val result = randomQuestionsInteractor.execute(
                    count = settingsUiState.settingsState.questionsCount,
                    tags = settingsUiState.settingsState.tags.toListQuestionTag(),
                )

                // navigate to quiz
                withContext(Dispatchers.Main) {
                    when (result) {
                        is RunQuizInteractor.Result.Success -> {
                            _actions.emit(
                                Actions.MoveToQuiz(
                                    quizId = result.quiz.id,
                                )
                            )
                            uiState = uiState.copy(
                                isLoading = false,
                            )
                        }
                        is RunQuizInteractor.Result.Error -> {
                            uiState = uiState.copy(
                                isLoading = false,
                            )
                            _actions.emit(Actions.ShowError(text = result.message ?: ""))
                        }
                    }
                }
            }
        }
    }

    // TODO отрефакторить в нормальный отлов исключений через flow
    private fun getExceptionHandler() = CoroutineExceptionHandler { _, throwable ->
        viewModelScope.launch {
            withContext(Dispatchers.Main) {
                uiState = uiState.copy(
                    isLoading = false,
                )
                _actions.emit(Actions.ShowError(text = "Ошибка загрузки вопросов"))
            }
        }
    }

    fun loadPreferSettings() {
        uiState = uiState.copy(isLoading = true)
        viewModelScope.launch {
            withContext(Dispatchers.IO) {
                val result = preferQuizSettingsInteractor.execute()
                val newState = result.toUiState()

                withContext(Dispatchers.Main) {
                    uiState = uiState.copy(
                        isLoading = false,
                        settingsState = newState,
                    )

                    savedStateHandle[PREFER_STORAGE_KEY] = uiState
                }
            }
        }
    }

    fun onSetQuestionsLimit(limit: Int) {
        val previousState = savedStateHandle.get<ChooseQuizParamsUiState>(PREFER_STORAGE_KEY)
        previousState!!.settingsState.questionsCount = limit
        savedStateHandle[PREFER_STORAGE_KEY] = previousState
    }

    fun onFilterStateChanged(tagUiState: TagUiState) {
        val previousState = savedStateHandle.get<ChooseQuizParamsUiState>(PREFER_STORAGE_KEY)
        previousState!!.settingsState.tags.first { it.id == tagUiState.id }.checked = tagUiState.checked
        savedStateHandle[PREFER_STORAGE_KEY] = previousState
    }

    companion object {
        private const val PREFER_STORAGE_KEY = "PREFER_STORAGE_KEY"
    }
}

private fun UserPreferSettings.toUiState(): SettingsUiState {
    return SettingsUiState(
        questionsCount = this.questionsCount,
        tags = this.tags.map {
            TagUiState(
                name = it.title,
                id = it.id,
                checked = it.checked,
            )
        }
    )
}

private fun List<TagUiState>.toQuestionTag(): List<QuizPreferredTag> {
    return this.map {
        QuizPreferredTag(
            id = it.id,
            title = it.name,
            checked = it.checked,
        )
    }
}

private fun SettingsUiState.toPreferSettings(): UserPreferSettings {
    return UserPreferSettings(
        questionsCount = this.questionsCount,
        tags = this.tags.toQuestionTag(),
    )
}

private fun List<TagUiState>.toListQuestionTag(): List<QuestionTag> {
    return this.filter { it.checked }.map {
        QuestionTag.valueOf(it.id)
    }
}
