package com.gmail.borlandlp.itsobes.ui.quiz

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.gmail.borlandlp.itsobes.R

@Composable
@Preview
fun SourceQuestionUi(
    link: String = "www.ya.ru",
    onLinkClicked: (link: String) -> Unit = {},
) {
    Row(modifier = Modifier.fillMaxWidth().padding(top = 16.dp)) {
        Text(text = stringResource(R.string.source_title))
        Text(
            text = link,
            maxLines = 1,
            overflow = TextOverflow.Ellipsis,
            modifier = Modifier
                .clickable { onLinkClicked(link) }
                .padding(start = 16.dp),
        )
    }
}