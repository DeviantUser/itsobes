package com.gmail.borlandlp.itsobes.ui.quiz

sealed interface NavigationEvent {
    data class MoveToNextQuiz(
        val quizId: Int,
        val nextQuestionNum: Int,
    ) : NavigationEvent

    class MoveToMain : NavigationEvent
}