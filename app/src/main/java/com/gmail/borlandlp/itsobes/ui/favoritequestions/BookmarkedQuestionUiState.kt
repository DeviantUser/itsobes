package com.gmail.borlandlp.itsobes.ui.favoritequestions

import android.os.Parcel
import android.os.Parcelable

class BookmarkedQuestionUiState(
    val link: String,
    val title: String,
    val markUnixTime: Long,
    var bookmarked: Boolean,
) : Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readString()!!,
        parcel.readString()!!,
        parcel.readLong(),
        parcel.readInt() == 1,
    ) {}

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(link)
        parcel.writeString(title)
        parcel.writeLong(markUnixTime)
        parcel.writeInt(if (bookmarked) 1 else 0)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<BookmarkedQuestionUiState> {
        override fun createFromParcel(parcel: Parcel): BookmarkedQuestionUiState {
            return BookmarkedQuestionUiState(parcel)
        }

        override fun newArray(size: Int): Array<BookmarkedQuestionUiState?> {
            return arrayOfNulls(size)
        }
    }
}
