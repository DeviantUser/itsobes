package com.gmail.borlandlp.itsobes.ui.navigation

import android.annotation.SuppressLint
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.painter.Painter
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.gmail.borlandlp.itsobes.R
import com.gmail.borlandlp.itsobes.ui.theme.NavColor
import com.gmail.borlandlp.itsobes.ui.theme.SecondaryColor
import com.gmail.borlandlp.itsobes.ui.theme.TypographyPrimary
import com.gmail.borlandlp.itsobes.ui.theme.TypographySecondaryColor
import com.gmail.borlandlp.itsobes.utils.defaultBottomShadow


@Preview
@Composable
@SuppressLint("ModifierParameter")
fun SettingsNavigationItem(
    selected: Boolean = false,
    onClick: () -> Unit = {},
    modifier: Modifier = Modifier,
) {
    BottomNavigationItem(
        icon = if (selected) {
            painterResource(id = R.drawable.ic_gears_white)
        } else {
            painterResource(id = R.drawable.ic_gears_blue)
        },
        text = "Настройки",
        selected = selected,
        onClick = onClick,
        modifier = modifier,
    )
}

@Preview
@Composable
@SuppressLint("ModifierParameter")
fun FavoriteQuestionsNavigationItem(
    selected: Boolean = false,
    onClick: () -> Unit = {},
    modifier: Modifier = Modifier,
) {
    BottomNavigationItem(
        icon = if (selected) {
            painterResource(id = R.drawable.ic_bookmark_white)
        } else {
            painterResource(id = R.drawable.ic_bookmark_stroke)
        },
        text = "Избранное",
        selected = selected,
        onClick = onClick,
        modifier = modifier,
    )
}

@Preview
@Composable
@SuppressLint("ModifierParameter")
fun QuizNavigationItem(
    selected: Boolean = false,
    onClick: () -> Unit = {},
    modifier: Modifier = Modifier,
) {
    BottomNavigationItem(
        icon = if (selected) {
            painterResource(id = R.drawable.ic_cube_white)
        } else {
            painterResource(id = R.drawable.ic_cube_blue)
        },
        text = "Квиз",
        selected = selected,
        onClick = onClick,
        modifier = modifier,
    )
}

@Composable
@Preview
@SuppressLint("ModifierParameter")
fun BottomNavigationItem(
    icon: Painter = painterResource(id = R.drawable.ic_cube_blue),
    text: String = "Квиз",
    selected: Boolean = false,
    onClick: () -> Unit = {},
    modifier: Modifier = Modifier,
) {
    Column(
        horizontalAlignment = Alignment.CenterHorizontally,
        modifier = Modifier
            .then(modifier)
            .clickable(
                onClick = onClick,
            ),
    ) {
        Image(
            painter = icon,
            contentDescription = "",
            modifier = Modifier
                .defaultBottomShadow(CircleShape)
                .clip(CircleShape)
                .background(if (selected) NavColor else SecondaryColor)
                .padding(8.dp)
                .height(26.dp)
                .width(26.dp)
        )

        Text(
            text = text,
            modifier = Modifier
                .align(Alignment.CenterHorizontally)
                .padding(top = 2.dp),
            color = if (!selected) TypographyPrimary else TypographySecondaryColor,
            fontSize = 12.sp,
        )
    }
}