package com.gmail.borlandlp.itsobes.ui.bookmarkedquestion

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material3.Scaffold
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalUriHandler
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import com.gmail.borlandlp.itsobes.R
import com.gmail.borlandlp.itsobes.ui.NoConnectionViewCompose
import com.gmail.borlandlp.itsobes.ui.ProgressComposeView
import com.gmail.borlandlp.itsobes.ui.actionbar.ActionBarWithNavigationBack
import com.gmail.borlandlp.itsobes.ui.quiz.AnswerTextUi
import com.gmail.borlandlp.itsobes.ui.quiz.QuestionTagsListUi
import com.gmail.borlandlp.itsobes.ui.quiz.QuizQuestion
import com.gmail.borlandlp.itsobes.ui.quiz.SourceQuestionUi

@Composable
fun FavoriteQuestionScreen(
    viewModel: BookmarkedQuestionViewModel = hiltViewModel(),
    navigateUp: () -> Unit,
) {
    if (viewModel.uiState.dataIsLoading) {
        ProgressComposeView(
            text = stringResource(id = R.string.loading_question),
        )
    } else if (viewModel.uiState.networkError) {
        NoConnectionViewCompose(
            onBtnReloadCLicked = { viewModel.loadBookmark() },
        )
    } else {
        val uriHandler = LocalUriHandler.current

        Scaffold(
            topBar = {
                ActionBarWithNavigationBack(
                    title = stringResource(R.string.bookmarked_questions),
                    navigateUp = navigateUp,
                    onClick = {
                        viewModel.switchBookmarkStatus()
                    },
                )
            },
        ) { paddingValues ->
            val scrollState = rememberScrollState()
            Column(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(
                        start = 16.dp,
                        end = 16.dp,
                    )
                    .padding(paddingValues = paddingValues)
                    .verticalScroll(scrollState),
            ) {
                Spacer(modifier = Modifier.fillMaxWidth().height(25.dp))

                QuizQuestion(
                    question = viewModel.uiState.state.title,
                    isBookmarked = viewModel.uiState.isBookmarked,
                    bookmarkClicked = {
                        viewModel.switchBookmarkStatus()
                    },
                )

                Spacer(modifier = Modifier
                    .fillMaxWidth()
                    .height(16.dp))

                QuestionTagsListUi(
                    tags = viewModel.uiState.state.tags,
                )

                SourceQuestionUi(
                    link = viewModel.uiState.state.link,
                    onLinkClicked = {
                        uriHandler.openUri(it)
                    },
                )

                AnswerTextUi(answer = viewModel.uiState.state.content)

                Spacer(modifier = Modifier
                    .fillMaxWidth()
                    .height(16.dp))
            }
        }
    }
}