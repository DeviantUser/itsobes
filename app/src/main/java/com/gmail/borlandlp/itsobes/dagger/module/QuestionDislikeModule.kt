package com.gmail.borlandlp.itsobes.dagger.module

import com.gmail.borlandlp.data.dislikequiz.ImplDislikedQuestionRepository
import com.gmail.borlandlp.data.dislikequiz.local.DislikeLocalStorage
import com.gmail.borlandlp.data.dislikequiz.local.DislikeQuestionDao
import com.gmail.borlandlp.data.questons.local.QuizDatabase
import com.gmail.borlandlp.domain.dislikequiz.DislikedQuestionRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ViewModelComponent

@Module(includes = [QuizDbModule::class])
@InstallIn(ViewModelComponent::class)
class QuestionDislikeModule {
    @Provides
    fun provideRepository(localStorage: DislikeLocalStorage): DislikedQuestionRepository {
        return ImplDislikedQuestionRepository(
            localStorage,
        )
    }

    @Provides
    fun getQuizDao(db: QuizDatabase): DislikeQuestionDao {
        return db.getDislikeQuizDao()
    }
}