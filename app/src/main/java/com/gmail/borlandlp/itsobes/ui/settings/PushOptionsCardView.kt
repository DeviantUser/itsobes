package com.gmail.borlandlp.itsobes.ui.settings

import android.content.Context
import androidx.compose.animation.core.animateFloatAsState
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.gestures.Orientation
import androidx.compose.foundation.gestures.draggable
import androidx.compose.foundation.gestures.rememberDraggableState
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.offset
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.outlined.Delete
import androidx.compose.material3.Card
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.Switch
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableFloatStateOf
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.graphicsLayer
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.gmail.borlandlp.itsobes.ui.theme.Gray
import com.gmail.borlandlp.itsobes.ui.theme.RedColor
import com.gmail.borlandlp.itsobes.ui.theme.SecondaryColor
import com.gmail.borlandlp.itsobes.ui.theme.TypographyPrimary
import com.gmail.borlandlp.itsobes.utils.dpToPx
import com.gmail.borlandlp.itsobes.utils.pxToDp

@Composable
@Preview(backgroundColor = 0xFFFFFFFF)
fun PushOptionsCardView(
    state: PushForDateUiState = PushForDateUiState(),
    onSetNewState: (newState: PushForDateUiState) -> Unit = {},
    onDeleteRow: (PushForDateUiState) -> Unit = {},
    context: Context = LocalContext.current
) {
    var switchState by remember {
        mutableStateOf(state.enabled)
    }

    val maxOffsetPx = 65.dp.dpToPx()

    var offsetX by remember { mutableFloatStateOf(0f) }
    val animation by animateFloatAsState(
        targetValue = offsetX,
        label = "Category list item animation"
    )
    val swiped by remember {
        mutableStateOf(false)
    }

    LaunchedEffect(key1 = swiped) {
        offsetX = if (swiped) {
            -maxOffsetPx
        } else {
            0f
        }
    }

    val draggableState = rememberDraggableState {
        when {
            offsetX + it > 0 -> offsetX = 0f
            offsetX + it < -maxOffsetPx -> offsetX = -maxOffsetPx
            else -> offsetX += it
        }
    }

    Box {
        Card(
            shape = RoundedCornerShape(24.dp),
            colors = CardDefaults.cardColors()
                .copy(containerColor = if (switchState) SecondaryColor else Gray),
            modifier = Modifier
                .fillMaxWidth()
                .draggable(
                    state = draggableState,
                    orientation = Orientation.Horizontal,
                    onDragStopped = {
                        offsetX = if (offsetX >= -maxOffsetPx / 2f) {
                            0f
                        } else {
                            -maxOffsetPx
                        }
                    }
                )
                .graphicsLayer {
                    translationX = offsetX
                }
        ) {

            Row(
                horizontalArrangement = Arrangement.SpaceBetween,
                modifier = Modifier.fillMaxWidth()
            ) {
                ClockFaceView(
                    initialHours = state.hours,
                    initialMinutes = state.minutes,
                    stateChanged = { newHours: Int, newMinutes: Int ->
                        state.hours = newHours
                        state.minutes = newMinutes
                        onSetNewState(state)
                    },
                    context = context,
                    modifier = Modifier
                        .padding(top = 16.dp, start = 16.dp),
                    isDisabled = !state.enabled,
                )

                Switch(
                    checked = switchState,
                    onCheckedChange = {
                        switchState = it
                        state.enabled = it
                        onSetNewState(state)
                    },
                    modifier = Modifier
                        .padding(top = 16.dp, end = 16.dp)
                )
            }

            HorizontalChipGroup(
                modifier = Modifier
                    .padding(top = 10.dp, start = 16.dp, end = 16.dp, bottom = 16.dp)
                    .fillMaxWidth(),
                items = state.state,
                onStateChanged = {
                    onSetNewState(state)
                }
            )
        }

        Row(
            horizontalArrangement = Arrangement.spacedBy(8.dp),
            modifier = Modifier
                .align(Alignment.CenterEnd)
                .offset(
                    x = (maxOffsetPx + animation).pxToDp() + (16.dp * ((maxOffsetPx + animation) / maxOffsetPx))
                )
        ) {
            IconButton(
                onClick = {
                    onDeleteRow(state)
                },
                modifier = Modifier
                    .size(
                        width = 50.dp,
                        height = 64.dp,
                    )
                    .background(
                        color = RedColor,
                        shape = RoundedCornerShape(16.dp),
                    )
                    .border(
                        width = 1.dp,
                        color = TypographyPrimary,
                        shape = RoundedCornerShape(16.dp),
                    )
            ) {
                Icon(
                    imageVector = Icons.Outlined.Delete,
                    contentDescription = null,
                    modifier = Modifier
                        .size(24.dp),
                )
            }
        }
    }
}