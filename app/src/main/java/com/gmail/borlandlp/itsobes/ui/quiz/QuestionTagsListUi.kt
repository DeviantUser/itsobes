package com.gmail.borlandlp.itsobes.ui.quiz

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.ExperimentalLayoutApi
import androidx.compose.foundation.layout.FlowRow
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.gmail.borlandlp.itsobes.R

@OptIn(ExperimentalLayoutApi::class)
@Composable
@Preview
fun QuestionTagsListUi(
    tags: List<String> = emptyList(),
) {
    FlowRow(
        modifier = Modifier.padding(top = 5.dp),
        horizontalArrangement = Arrangement.spacedBy(8.dp),
        verticalArrangement = Arrangement.spacedBy(8.dp),
    ) {
        Text(
            text = stringResource(R.string.tags),
            modifier = Modifier.align(Alignment.CenterVertically)
        )

        var i = 0
        repeat(tags.size) {
            TagUI(tags[i++])
        }
    }
}