package com.gmail.borlandlp.itsobes.utils

import android.app.Activity
import android.app.NotificationManager
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Build
import android.provider.Settings.ACTION_APPLICATION_DETAILS_SETTINGS
import androidx.core.app.ActivityCompat
import androidx.core.app.NotificationManagerCompat
import androidx.core.content.ContextCompat
import com.google.android.material.R
import com.google.android.material.dialog.MaterialAlertDialogBuilder


class PushUtils {
    companion object {
        fun areNotificationsEnabled(context: Context): Boolean {
            return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                val manager = context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
                if (!manager.areNotificationsEnabled()) {
                    return false
                }
                val channels = manager.notificationChannels
                for (channel in channels) {
                    if (channel.importance == NotificationManager.IMPORTANCE_NONE) {
                        return false
                    }
                }
                true
            } else {
                NotificationManagerCompat.from(context).areNotificationsEnabled()
            }
        }

        fun requestPushPermission(activity: Activity) {
            if (Build.VERSION.SDK_INT >= 33) {
                if (ContextCompat.checkSelfPermission(activity, android.Manifest.permission.POST_NOTIFICATIONS) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(activity, arrayOf(android.Manifest.permission.POST_NOTIFICATIONS),101)
                }
            } else {
                MaterialAlertDialogBuilder(activity, R.style.MaterialAlertDialog_Material3)
                    .setTitle(activity.getString(com.gmail.borlandlp.itsobes.R.string.grant_permissions_title))
                    .setMessage(activity.getString(com.gmail.borlandlp.itsobes.R.string.grant_permissions_text))
                    .setPositiveButton(activity.getString(com.gmail.borlandlp.itsobes.R.string.move_to_settings)) { _, _ ->
                        val intent = Intent(ACTION_APPLICATION_DETAILS_SETTINGS)
                        intent.data = Uri.parse("package:${activity.packageName}")
                        activity.startActivity(intent)
                    }
                    .setNegativeButton(activity.getString(com.gmail.borlandlp.itsobes.R.string.cancel), null)
                    .show()
            }
        }
    }
}