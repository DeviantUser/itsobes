package com.gmail.borlandlp.itsobes.dagger.module.core

import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ActivityComponent
import dagger.hilt.android.components.ViewModelComponent
import okhttp3.OkHttpClient
import retrofit2.Converter
import retrofit2.Retrofit

@Module(includes = [OkHttpModule::class])
@InstallIn(ViewModelComponent::class, ActivityComponent::class)
class RetrofitModule {
    @Provides
    fun getRetrofit(
        okHttpClient: OkHttpClient,
        kotlinConverterFactory: Converter.Factory
    ): Retrofit {
        return Retrofit.Builder()
            .client(okHttpClient)
            .addConverterFactory(kotlinConverterFactory)
            .baseUrl("https://tomato-development.ru")
            .build()
    }
}