package com.gmail.borlandlp.itsobes.ui.settings

import android.app.Activity
import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import androidx.compose.ui.platform.LocalContext
import androidx.lifecycle.viewmodel.compose.viewModel
import com.gmail.borlandlp.domain.pushservice.EnablePushNotificationsUseCase
import com.gmail.borlandlp.domain.pushservice.GetPushSettingsUseCase
import dagger.BindsInstance
import dagger.hilt.DefineComponent
import dagger.hilt.EntryPoint
import dagger.hilt.EntryPoints
import dagger.hilt.InstallIn
import dagger.hilt.android.EntryPointAccessors
import dagger.hilt.android.components.ActivityComponent
import javax.inject.Inject

class SettingsScreenFactory @Inject constructor() {
    @Composable
    fun Content() {
        val activity = LocalContext.current as Activity
        val viewModelFactory = remember {
            EntryPointAccessors.fromActivity(
                activity,
                UseCaseBuilderProvider::class.java,
            )
        }
        val component = viewModelFactory.getEnablePushNotificationBuilder()
            .setActivity(activity)
            .build()
        val enablePushNotificationsUseCase = EntryPoints.get(
            component,
            UseCaseProvider::class.java,
        )

        val factory = EntryPointAccessors.fromActivity(
            LocalContext.current as Activity,
            ViewModelFactoryProvider::class.java,
        ).bookDetailsViewModelFactory()
        val viewModel = viewModel<SettingsViewModel>(
            factory = SettingsViewModel.providesFactory(
                assistedFactory = factory,
                enablePushNotificationsUseCase = enablePushNotificationsUseCase.getUseCase(),
                getPushSettingsUseCase = enablePushNotificationsUseCase.getPushSettingsUseCase(),
            ),
        )

        SettingsScreen(viewModel)
    }
}

@DefineComponent(parent = ActivityComponent::class)
interface EnablePushNotificationComponent {
    @DefineComponent.Builder
    interface Builder {
        fun setActivity(@BindsInstance activity: Activity): Builder
        fun build(): EnablePushNotificationComponent
    }
}

@EntryPoint
@InstallIn(EnablePushNotificationComponent::class)
interface UseCaseProvider {
    fun getUseCase(): EnablePushNotificationsUseCase
    fun getPushSettingsUseCase(): GetPushSettingsUseCase
}

@EntryPoint
@InstallIn(ActivityComponent::class)
interface UseCaseBuilderProvider {
    fun getEnablePushNotificationBuilder(): EnablePushNotificationComponent.Builder
}

@EntryPoint
@InstallIn(ActivityComponent::class)
interface ViewModelFactoryProvider {
    fun bookDetailsViewModelFactory(): SettingsViewModel.ArticlesFeedViewModelFactory
}
