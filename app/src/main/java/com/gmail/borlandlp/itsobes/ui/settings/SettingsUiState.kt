package com.gmail.borlandlp.itsobes.ui.settings

import com.google.common.collect.ImmutableList

data class SettingsUiState(
    val data: ImmutableList<PushForDateUiState> = ImmutableList.of(),
    val enabled: Boolean = false,
    val isLoading: Boolean = false,
    val errorMessage: String? = null,
    val networkError: Boolean = false,
    val needOpenRequestPushPermissions: Boolean = false,
)
