package com.gmail.borlandlp.itsobes.ui.choosequizparams

import android.os.Parcel
import android.os.Parcelable


data class ChooseQuizParamsUiState(
    val isLoading: Boolean = false,
    val isError: Boolean = false,
    val networkError: Boolean = false,
    val settingsState: SettingsUiState = SettingsUiState(10, emptyList()),
) : Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readByte() != 0.toByte(),
        parcel.readByte() != 0.toByte(),
        parcel.readByte() != 0.toByte(),
        parcel.readParcelable(SettingsUiState::class.java.classLoader)!!
    ) {}

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeByte(if (isLoading) 1 else 0)
        parcel.writeByte(if (isError) 1 else 0)
        parcel.writeByte(if (networkError) 1 else 0)
        parcel.writeParcelable(settingsState, flags)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<ChooseQuizParamsUiState> {
        override fun createFromParcel(parcel: Parcel): ChooseQuizParamsUiState {
            return ChooseQuizParamsUiState(parcel)
        }

        override fun newArray(size: Int): Array<ChooseQuizParamsUiState?> {
            return arrayOfNulls(size)
        }
    }
}
