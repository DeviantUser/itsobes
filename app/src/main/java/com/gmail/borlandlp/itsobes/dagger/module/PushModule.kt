package com.gmail.borlandlp.itsobes.dagger.module

import android.app.Activity
import android.content.Context
import android.content.SharedPreferences
import com.gmail.borlandlp.data.DeviceContext
import com.gmail.borlandlp.data.pushservice.ImplPushCompanyService
import com.gmail.borlandlp.data.pushservice.ImplPushSettingsService
import com.gmail.borlandlp.data.pushservice.ImplPushTokenService
import com.gmail.borlandlp.data.pushservice.PushCampaignApi
import com.gmail.borlandlp.data.pushservice.PushSettingsApi
import com.gmail.borlandlp.data.pushservice.PushTokenApi
import com.gmail.borlandlp.domain.pushservice.LocalPushService
import com.gmail.borlandlp.domain.pushservice.PushCompanyService
import com.gmail.borlandlp.domain.pushservice.PushSettingsService
import com.gmail.borlandlp.domain.pushservice.PushTokenService
import com.gmail.borlandlp.itsobes.dagger.module.core.RetrofitModule
import com.gmail.borlandlp.itsobes.service.push.ComposeLocalPushService
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ActivityComponent
import dagger.hilt.android.components.ViewModelComponent
import dagger.hilt.android.qualifiers.ApplicationContext
import retrofit2.Retrofit
import javax.inject.Named

@Module(includes = [RetrofitModule::class])
@InstallIn(ViewModelComponent::class, ActivityComponent::class)
class PushModule {
    @Provides
    fun getPushSettingsService(
        pushSettingsApi: PushSettingsApi,
        deviceContext: DeviceContext,
    ): PushSettingsService {
        return ImplPushSettingsService(
            api = pushSettingsApi,
            deviceContext = deviceContext,
        )
    }

    @Provides
    fun getPushCompanyService(
        api: PushCampaignApi,
        deviceContext: DeviceContext,
    ): PushCompanyService {
        return ImplPushCompanyService(
            remoteApi = api,
            deviceContext = deviceContext,
        )
    }

    @Provides
    fun getPushCampaignApi(retrofit: Retrofit): PushCampaignApi {
        return retrofit.create(PushCampaignApi::class.java)
    }

    @Provides
    fun getRemoteApi(retrofit: Retrofit): PushSettingsApi {
        return retrofit.create(PushSettingsApi::class.java)
    }

    @Provides
    fun getRemoteTokenApi(retrofit: Retrofit): PushTokenApi {
        return retrofit.create(PushTokenApi::class.java)
    }

    @Provides
    fun getTokenStorage(
        pushSettingsApi: PushTokenApi,
        deviceContext: DeviceContext,
        @Named("push_storage") sharedPreferences: SharedPreferences,
    ): PushTokenService {
        return ImplPushTokenService(
            api = pushSettingsApi,
            deviceContext = deviceContext,
            sharedPreferences = sharedPreferences,
        )
    }

    @Named("push_storage")
    @Provides
    fun provideSharedPreferences(
        @ApplicationContext context: Context
    ): SharedPreferences {
        return context.getSharedPreferences("push_storage", Context.MODE_PRIVATE)
    }

    @Provides
    fun getComposeLocalPushService(activity: Activity): LocalPushService {
        return ComposeLocalPushService(
            activity = activity,
        )
    }
}