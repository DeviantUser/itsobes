package com.gmail.borlandlp.itsobes.utils

import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.AbstractSavedStateViewModelFactory
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel

@SuppressWarnings("unchecked")
inline fun <reified T : ViewModel> Fragment.getViewModel(
    defArgs: Bundle?,
    crossinline provider: (handle: SavedStateHandle) -> T,
) = viewModels<T> {
        object : AbstractSavedStateViewModelFactory(this@getViewModel, defArgs ?: arguments) {
            override fun <T : ViewModel> create(
                key: String,
                modelClass: Class<T>,
                handle: SavedStateHandle
            ) = provider(handle) as T
        }
    }