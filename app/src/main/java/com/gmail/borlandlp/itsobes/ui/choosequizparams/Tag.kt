package com.gmail.borlandlp.itsobes.ui.choosequizparams

import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.ripple.rememberRipple
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.gmail.borlandlp.itsobes.ui.theme.InactiveColor
import com.gmail.borlandlp.itsobes.ui.theme.PrimaryColor
import com.gmail.borlandlp.itsobes.ui.theme.SecondaryColor
import com.gmail.borlandlp.itsobes.ui.theme.TypographyPrimary
import com.gmail.borlandlp.itsobes.ui.theme.TypographySecondaryColor
import com.gmail.borlandlp.itsobes.utils.defaultBottomShadow

@Composable
@Preview
fun Tag(
    tag: TagUiState = TagUiState("Test", false, ""),
    onStateChanged: (newTagState: TagUiState) -> Unit = {},
) {
    var selected by remember { mutableStateOf(tag.checked) }

    val modifier = Modifier
    val shape = RoundedCornerShape(50.dp)

    Text(
        text = tag.name,
        fontSize = 16.sp,
        color = if (selected) TypographyPrimary else TypographySecondaryColor,
        modifier = modifier
            .then(
                if (!selected) {
                    modifier.defaultBottomShadow(shape)
                        .clickable(
                            onClick = {
                                selected = !selected
                                tag.checked = selected
                                onStateChanged(tag)
                            },
                            interactionSource = remember { MutableInteractionSource() },
                            indication = rememberRipple(),
                        )
                        .background(InactiveColor)
                        .padding(1.dp)
                        .clip(shape)
                        .background(SecondaryColor)
                        .padding(
                            start = 10.dp,
                            top = 4.dp,
                            end = 10.dp,
                            bottom = 4.dp,
                        )
                } else {
                    modifier
                        .clickable(
                            onClick = {
                                selected = !selected
                                tag.checked = selected
                                onStateChanged(tag)
                            },
                            interactionSource = remember { MutableInteractionSource() },
                            indication = rememberRipple(),
                        )
                        .clip(shape)
                        .background(PrimaryColor)
                        .padding(
                            start = 11.dp,
                            top = 5.dp,
                            end = 11.dp,
                            bottom = 5.dp,
                        )
                }
            )
        ,
    )
}