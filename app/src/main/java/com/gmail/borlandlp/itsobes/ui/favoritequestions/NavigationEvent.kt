package com.gmail.borlandlp.itsobes.ui.favoritequestions

sealed interface NavigationEvent {
    data class MoveToQuestion(
        val questionLink: String,
    ) : NavigationEvent
}