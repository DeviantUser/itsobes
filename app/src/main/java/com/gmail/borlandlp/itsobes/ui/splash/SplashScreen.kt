package com.gmail.borlandlp.itsobes.ui.splash

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.width
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.airbnb.lottie.compose.LottieAnimation
import com.airbnb.lottie.compose.LottieCompositionSpec
import com.airbnb.lottie.compose.rememberLottieComposition
import com.gmail.borlandlp.itsobes.R
import com.gmail.borlandlp.itsobes.ui.theme.NavColor

@Composable
@Preview
fun SplashScreen() {
    Box(modifier = Modifier
        .fillMaxSize()
        .background(NavColor),
    ) {
//        val composition by rememberLottieComposition(LottieCompositionSpec.RawRes(R.raw.animation_drawer))
//        LottieAnimation(
//            composition = composition,
//            iterations = Int.MAX_VALUE,
//            speed = 0.5F,
//            modifier = Modifier
//                .fillMaxWidth()
//                .height(600.dp)
//                .align(Alignment.BottomCenter),
//        )

        Image(
            painter = painterResource(id = R.drawable.app_logo),
            contentDescription = "Logo of application",
            modifier = Modifier
                .height(255.dp)
                .width(243.dp)
                .align(Alignment.Center),
        )
    }
}