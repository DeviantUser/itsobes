package com.gmail.borlandlp.itsobes.ui.settings

import android.widget.Toast
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.itemsIndexed
import androidx.compose.material3.Scaffold
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import androidx.constraintlayout.compose.ConstraintLayout
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.lifecycle.Lifecycle
import com.gmail.borlandlp.itsobes.R
import com.gmail.borlandlp.itsobes.ui.NoConnectionViewCompose
import com.gmail.borlandlp.itsobes.ui.OptionalRationalPermissionDialog
import com.gmail.borlandlp.itsobes.ui.ProgressComposeView
import com.gmail.borlandlp.itsobes.ui.actionbar.SimpleActionBarUi
import com.gmail.borlandlp.itsobes.utils.OnLifecycleEvent
import kotlinx.coroutines.launch

@Composable
fun SettingsScreen(
    viewModel: SettingsViewModel = hiltViewModel(),
) {
    val scope = rememberCoroutineScope()
    val context = LocalContext.current
    LaunchedEffect(context) {
        scope.launch {
            viewModel.errorMessage.collect {
                if (it.isNotEmpty()) {
                    Toast.makeText(
                        context,
                        it,
                        Toast.LENGTH_LONG,
                    ).show()
                }
            }
        }
    }

    OnLifecycleEvent(onEvent = { _, event ->
        if (event == Lifecycle.Event.ON_PAUSE && viewModel.uiState.needOpenRequestPushPermissions) {
            viewModel.onDismissPushWindow()
        }
    })

    if (viewModel.uiState.isLoading) {
        ProgressComposeView(
            text = stringResource(R.string.load_settings),
        )
    } else if (viewModel.uiState.networkError) {
        NoConnectionViewCompose(
            onBtnReloadCLicked = { viewModel.getSettings() },
        )
    } else {
        Scaffold(
            topBar = {
                SimpleActionBarUi(
                    title = stringResource(R.string.options_screen_title),
                )
            },
        ) {
            ConstraintLayout(
                modifier = Modifier.fillMaxSize(),
            ) {
                val (itemsContainer, button) = createRefs()

                LazyColumn(
                    horizontalAlignment = Alignment.CenterHorizontally,
                    verticalArrangement = Arrangement.spacedBy(16.dp),
                    modifier = Modifier
                        .fillMaxSize()
                        .background(Color.White)
                        .padding(start = 16.dp, end = 16.dp)
                        .padding(paddingValues = it)
                        .constrainAs(itemsContainer) {
                            top.linkTo(parent.top)
                        },
                ) {
                    itemsIndexed(
                        items = viewModel.uiState.data,
                        itemContent = { index, state ->
                            if (index == 0) {
                                Spacer(
                                    modifier = Modifier
                                        .fillMaxWidth()
                                        .height(16.dp),
                                )

                                ControlNotificationsView(
                                    enabled = viewModel.uiState.enabled,
                                    onStateChanged = {
                                        viewModel.changeNotificationsState(it)
                                    },
                                )

                                Spacer(
                                    modifier = Modifier
                                        .fillMaxWidth()
                                        .height(16.dp),
                                )
                            }

                            PushOptionsCardView(
                                state = state,
                                onSetNewState = {
                                    viewModel.setPushForDate(it)
                                },
                                onDeleteRow = {
                                    viewModel.removePushCampaign(it)
                                },
                                context = context,
                            )

                            if (viewModel.uiState.data.lastIndex == index) {
                                Spacer(
                                    modifier = Modifier
                                        .fillMaxWidth()
                                        .height(16.dp),
                                )
                            }
                        },
                    )
                }

                if (viewModel.uiState.data.size < 5) {
                    val imageSize = 70.dp
                    Image(
                        painter = painterResource(id = R.drawable.ic_plus),
                        contentDescription = "Add a reminder about completing the quiz",
                        modifier = Modifier
                            .clickable { viewModel.addNewPushCompany() }
                            .constrainAs(button) {
                                bottom.linkTo(parent.bottom)
                                end.linkTo(parent.end)
                                start.linkTo(parent.start)
                            }
                            .height(imageSize)
                            .width(imageSize)
                            .padding(bottom = 16.dp),
                    )
                }
            }
        }
    }

    if (viewModel.uiState.needOpenRequestPushPermissions) {
        OptionalRationalPermissionDialog() {
            viewModel.onDismissPushWindow()
        }
    }
}