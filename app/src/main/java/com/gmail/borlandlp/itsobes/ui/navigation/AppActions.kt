package com.gmail.borlandlp.itsobes.ui.navigation

import androidx.navigation.NavHostController
import java.net.URLEncoder
import java.nio.charset.StandardCharsets

class AppActions(
    private val navController: NavHostController,
    private val routes: AppDestinations
) {
    val navigateToQuiz: (quizId: Int) -> Unit = { quizId: Int ->
        navController.navigate("${routes.QUIZ_ROUTE_SCREEN}/${quizId}/0")
    }

    val navigateToQuizPage: (quizId: Int, page: Int) -> Unit = { quizId: Int, page: Int ->
        navController.navigate("${routes.QUIZ_ROUTE_SCREEN}/${quizId}/${page}")
    }

    val navigateToMain: (popBackstack: Boolean) -> Unit = {
        if (it) {
            navController.popBackStack()
        }
        navController.navigate(routes.QUIZ_PARAMS_SCREEN)
    }

    val navigateToFavoriteQuestions: () -> Unit = {
        navController.navigate(routes.FAVORITE_QUESTIONS_SCREEN)
    }

    val navigateToFavorite: (questionUrl: String) -> Unit = { questionUrl ->
        val encodedUrl = URLEncoder.encode(questionUrl, StandardCharsets.UTF_8.toString())
        navController.navigate("${routes.FAVORITE_QUESTION_SCREEN}/${encodedUrl}")
    }

    val navigateToSettings: () -> Unit = {
        navController.navigate(routes.SETTINGS_SCREEN)
    }

    val navigateUp: () -> Unit = {
        navController.navigateUp()
    }
}