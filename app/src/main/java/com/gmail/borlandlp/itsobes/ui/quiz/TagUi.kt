package com.gmail.borlandlp.itsobes.ui.quiz

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.gmail.borlandlp.itsobes.ui.theme.SecondaryColor
import com.gmail.borlandlp.itsobes.utils.defaultBottomShadow

@Composable
@Preview
fun TagUI(tag: String = "test") {
    val shape = RoundedCornerShape(20.dp)
    Text(
        text = tag,
        modifier = Modifier
            .padding(start = 8.dp)
            .defaultBottomShadow(shape = shape)
            .clip(shape)
            .background(SecondaryColor)
            .padding(
                start = 12.dp,
                top = 6.dp,
                end = 12.dp,
                bottom = 6.dp,
            ),
    )
}