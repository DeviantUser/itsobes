package com.gmail.borlandlp.itsobes.ui.settings

import androidx.annotation.IntRange
import com.gmail.borlandlp.domain.pushservice.DayOfWeek

class PushForDateUiState(
    val campaignId: Int = 0,
    @IntRange(from = 0, to = 23)
    var hours: Int = 0,
    @IntRange(from = 0, to = 59)
    var minutes: Int = 0,
    var enabled: Boolean = false,
    val state: List<PushDayOfWeekUi> = emptyList(),
)

class PushDayOfWeekUi(
    var enabled: Boolean,
    val day: DayOfWeek,
)