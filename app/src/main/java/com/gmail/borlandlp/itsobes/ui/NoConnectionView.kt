package com.gmail.borlandlp.itsobes.ui

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.material3.Button
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.airbnb.lottie.compose.LottieAnimation
import com.airbnb.lottie.compose.LottieCompositionSpec
import com.airbnb.lottie.compose.rememberLottieComposition
import com.gmail.borlandlp.itsobes.R


@Composable
@Preview
fun NoConnectionViewCompose(
    onBtnReloadCLicked: () -> Unit = {},
) {
    Box(
        modifier = Modifier
            .fillMaxSize()
            .background(colorResource(id = R.color.white)),
        contentAlignment = Alignment.Center,
    ) {
        Column (horizontalAlignment = Alignment.CenterHorizontally){
            val composition by rememberLottieComposition(LottieCompositionSpec.RawRes(R.raw.no_internet_animation))
            LottieAnimation(
                composition = composition,
                modifier = Modifier
                    .height(150.dp)
                    .width(150.dp),
                iterations = Integer.MAX_VALUE,
                speed = 0.5F,
            )
            Text(
                text = stringResource(id = R.string.no_connection),
                color = colorResource(id = R.color.black),
                fontSize = 15.sp,
            )
            Button(
                onClick = { onBtnReloadCLicked() },
                modifier = Modifier.padding(top = 35.dp)
            ) {
                Text(text = stringResource(id = R.string.load_page_again))
            }
        }
    }
}