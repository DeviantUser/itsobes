package com.gmail.borlandlp.itsobes.ui.theme

import androidx.compose.ui.graphics.Color

val Purple80 = Color(0xFFD0BCFF)
val PurpleGrey80 = Color(0xFFCCC2DC)
val Pink80 = Color(0xFFEFB8C8)

val Purple40 = Color(0xFF6650a4)
val PurpleGrey40 = Color(0xFF625b71)
val Pink40 = Color(0xFF7D5260)
val Black = Color(0xFF0D141C)
val PrimaryBlue = Color(0xFF1c93f3)
val Gray = Color(0xFFEBEBEB)

val White = Color(0xFFFFFFFF)
val White90percent = Color(0xE6FFFFFF)
val White60percent = Color(0x99FFFFFF)
val White40percent = Color(0x66FFFFFF)
val SecondaryColor = Color(0xFFFBECD7)
val PrimaryColor = Color(0xFFFF9F1C)
val NavColor = Color(0xFF186075)
val NavColor60alpha = Color(0x99186075)
val TypographyPrimary = Color(0xFF38281B)
val TypographySecondaryColor = Color(0xFF805B22)
val RedColor = Color(0x4DFF0000)
val InactiveColor = Color(0xFFfbebd7)
val InactiveBorderColor = Color(0xFFDBC3A1)
