package com.gmail.borlandlp.itsobes.ui.quiz

import android.text.SpannableStringBuilder
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import androidx.core.text.HtmlCompat
import com.gmail.borlandlp.itsobes.utils.toAnnotatedString

@Composable
fun AnswerTextUi(answer: String) {
    val spannableString = SpannableStringBuilder(answer).toString()
    val spanned = HtmlCompat.fromHtml(spannableString, HtmlCompat.FROM_HTML_MODE_COMPACT)

    Text(
        text = spanned.toAnnotatedString(),
        modifier = Modifier
            .fillMaxWidth()
            .padding(top = 16.dp)
    )
}