package com.gmail.borlandlp.itsobes.ui.favoritequestions

data class FavoriteQuestionsUiState(
    val bookmarksUiState: List<BookmarkedQuestionUiState> = emptyList(),
    val pageIsLoading: Boolean = false,
    val errorMessage: String = "",
    val networkError: Boolean = false,
)