package com.gmail.borlandlp.itsobes.dagger.module.core

import com.google.firebase.crashlytics.ktx.crashlytics
import com.google.firebase.ktx.Firebase
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import kotlinx.coroutines.CoroutineExceptionHandler

@Module
@InstallIn(SingletonComponent::class)
class Android {
    @Provides
    fun getExceptionHandler() = CoroutineExceptionHandler { _, throwable ->
        Firebase.crashlytics.recordException(throwable)
    }
}