package com.gmail.borlandlp.itsobes

import android.os.Bundle
import androidx.activity.compose.setContent
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.app.AppCompatDelegate
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.ui.Modifier
import androidx.lifecycle.lifecycleScope
import com.gmail.borlandlp.domain.pushservice.RefreshPushTokenInteractor
import com.gmail.borlandlp.itsobes.ui.navigation.AppNavigation
import com.gmail.borlandlp.itsobes.ui.theme.ItSobesComposeTheme
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.launch
import kotlinx.coroutines.plus
import javax.inject.Inject

// TODO LeakCanary
// TODO Google Cloud Messaging
// Перехватывать открытие ссылок в квизе на домен itsobes для открытия страницы с просмотра ответа внутри приложения?
// preparing for a job interview for middle android developers IT man in glasses book https://perchance.org/ai-icon-generator
// Добавить Relations для Room для дао получение дизлайкнутых + добавленных в избранное
// Добавить автообновление токена FcmListener + отправку токена при захождении на страницу
// https://github.com/DEBAGanov/interview_questions/blob/main/Cобеседование%20по%20Java.%20Patterns.md
// https://dzen.ru/android_junior
// Вернуть модалку с предложением о включении пушей
// Если пуши выключены - отображать экран, что сначала для пуш-уведомлений нужно включить пуши
// Переделать навигацию на https://medium.com/androiddevelopers/navigation-compose-meet-type-safety-e081fb3cf2f8 + https://developer.android.com/jetpack/androidx/releases/navigation#2.8.0-alpha08
// Проверить страницу квиза и настроек на нормальную работу без интернета
// Добавить функционал для незрячих людей или людей, с проблемами
@AndroidEntryPoint
class MainActivity : AppCompatActivity() {
    @Inject
    lateinit var refreshPushTokenInteractor: RefreshPushTokenInteractor
    @Inject
    lateinit var exceptionHandler: CoroutineExceptionHandler

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)

        val scope = lifecycleScope + SupervisorJob() + exceptionHandler
        scope.launch {
            refreshPushTokenInteractor.invoke()
                .collect {}
        }

        setContent {
            ItSobesComposeTheme(darkTheme = false, dynamicColor = false) {
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    AppNavigation()
                }
            }
        }
    }
}
