package com.gmail.borlandlp.itsobes.ui.choosequizparams

import android.os.Parcel
import android.os.Parcelable


class SettingsUiState (
    var questionsCount: Int,
    var tags: List<TagUiState>,
) : Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readInt(),
        parcel.createTypedArrayList(TagUiState)!!.toList()
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(questionsCount)
        parcel.writeTypedList(tags)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<SettingsUiState> {
        override fun createFromParcel(parcel: Parcel): SettingsUiState {
            return SettingsUiState(parcel)
        }

        override fun newArray(size: Int): Array<SettingsUiState?> {
            return arrayOfNulls(size)
        }
    }

}

class TagUiState (
    val name: String,
    var checked: Boolean,
    val id: String,
) : Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readString()!!,
        parcel.readByte() != 0.toByte(),
        parcel.readString()!!,
    ) {}

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(name)
        parcel.writeByte(if (checked) 1 else 0)
        parcel.writeString(id)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<TagUiState> {
        override fun createFromParcel(parcel: Parcel): TagUiState {
            return TagUiState(parcel)
        }

        override fun newArray(size: Int): Array<TagUiState?> {
            return arrayOfNulls(size)
        }
    }
}
