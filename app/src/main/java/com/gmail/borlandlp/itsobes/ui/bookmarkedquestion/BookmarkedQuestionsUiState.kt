package com.gmail.borlandlp.itsobes.ui.bookmarkedquestion

data class BookmarkedQuestionsUiState(
    val dataIsLoading: Boolean = false,
    val networkError: Boolean = false,
    val state: BookmarkUiState = BookmarkUiState(),
    val isBookmarked: Boolean = true,
)