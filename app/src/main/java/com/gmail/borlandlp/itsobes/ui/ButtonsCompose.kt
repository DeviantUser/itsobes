package com.gmail.borlandlp.itsobes.ui

import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.gmail.borlandlp.itsobes.ui.theme.Black
import com.gmail.borlandlp.itsobes.ui.theme.NavColor
import com.gmail.borlandlp.itsobes.ui.theme.PrimaryBlue
import com.gmail.borlandlp.itsobes.ui.theme.White
import com.gmail.borlandlp.itsobes.utils.defaultBottomShadow

@Composable
@Preview
fun FilledButtonUi(
    text: String = "Push me",
    onClick: () -> Unit = {},
) {
    val shape = RoundedCornerShape(50.dp)
    Button(
        onClick = onClick,
        content = { Text(text = text) },
        colors = ButtonDefaults.buttonColors(
            containerColor = NavColor,
            contentColor = White,
        ),
        shape = shape,
        modifier = Modifier
            .defaultBottomShadow(shape),
    )
}

@Composable
@Preview
fun TransparentButtonUi(
    text: String = "Push me",
    onClick: () -> Unit = {},
) {
    val cornerShapeWidth = 50.dp
    val shape = RoundedCornerShape(cornerShapeWidth)
    Button(
        onClick = onClick,
        content = { Text(text = text) },
        colors = ButtonDefaults.buttonColors(
            containerColor = White,
            contentColor = Black,
        ),
        border = BorderStroke(1.dp, PrimaryBlue),
        modifier = Modifier.defaultBottomShadow(shape),
    )
}