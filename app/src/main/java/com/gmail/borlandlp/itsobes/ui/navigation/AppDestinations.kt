package com.gmail.borlandlp.itsobes.ui.navigation

import android.os.Build
import android.os.Bundle
import android.os.Parcelable
import androidx.navigation.NavType
import kotlinx.parcelize.Parcelize
import kotlinx.serialization.KSerializer
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import kotlinx.serialization.descriptors.PrimitiveKind
import kotlinx.serialization.descriptors.PrimitiveSerialDescriptor
import kotlinx.serialization.descriptors.SerialDescriptor
import kotlinx.serialization.encodeToString
import kotlinx.serialization.encoding.Decoder
import kotlinx.serialization.encoding.Encoder
import kotlinx.serialization.json.Json

object AppDestinations {
    const val SPLASH_SCREEN = "SPLASH_SCREEN"

    // Default destination
    const val QUIZ_PARAMS_SCREEN = "quiz_params"

    // destination for searching actors
    const val QUIZ_ROUTE_SCREEN = "quiz"
    const val QUIZ_PARAM_ID = "quiz_id"
    const val QUIZ_PARAM_QUESTION_NUM = "quiz_question_num"

    const val FAVORITE_QUESTIONS_SCREEN = "favorite_questions_screen"

    const val FAVORITE_QUESTION_SCREEN = "favorite_question_screen"
    const val FAVORITE_QUESTION_PARAM_QUESTION_URL = "favorite_question_param_question_url"

    const val SETTINGS_SCREEN = "settings_screen"
}

@Serializable
object QuizParamsScreen

// <View quiz screen>
@Serializable
data class ViewQuizScreen(
    @Serializable(with = ViewQuizScreenParametersSerializer::class)
    val parameters: ViewQuizScreenParameters,
)

@Serializable
@Parcelize
data class ViewQuizScreenParameters(
    @SerialName("quiz_id")
    val quizId: Int,
    @SerialName("quiz_question_num")
    val questionIndex: Int = 0,
) : Parcelable

val ViewQuizScreenParametersType = object : NavType<ViewQuizScreenParameters>(
    isNullableAllowed = false
) {
    override fun get(bundle: Bundle, key: String): ViewQuizScreenParameters {
        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
            bundle.getParcelable(key, ViewQuizScreenParameters::class.java)!!
        } else {
            return bundle.getParcelable(key)!!
        }
    }

    override fun parseValue(value: String): ViewQuizScreenParameters {
        return Json.decodeFromString(value)
    }

    override fun put(bundle: Bundle, key: String, value: ViewQuizScreenParameters) {
        bundle.putParcelable(key, value)
    }
}

object ViewQuizScreenParametersSerializer : KSerializer<ViewQuizScreenParameters> {
    override val descriptor: SerialDescriptor = PrimitiveSerialDescriptor(
        "ViewQuizScreenParameters",
        PrimitiveKind.STRING,
    )

    override fun deserialize(decoder: Decoder): ViewQuizScreenParameters {
        return Json.decodeFromString<ViewQuizScreenParameters>(decoder.decodeString())

    }

    override fun serialize(encoder: Encoder, value: ViewQuizScreenParameters) {
        encoder.encodeString(Json.encodeToString(value))
    }
}

// </View quiz screen>

@Serializable
object FavoritesScreen

@Serializable
data class ViewFavoriteQuestionScreen(
    @Serializable(with = ViewFavoriteQuestionScreenParametersSerializer::class)
    val parameters: ViewFavoriteQuestionScreenParameters,
)

@Parcelize
data class ViewFavoriteQuestionScreenParameters(
    val questionUrl: String,
) : Parcelable

object ViewFavoriteQuestionScreenParametersSerializer : KSerializer<ViewFavoriteQuestionScreenParameters> {
    override val descriptor: SerialDescriptor = PrimitiveSerialDescriptor(
        "ViewFavoriteQuestionScreenParameters",
        PrimitiveKind.STRING,
    )

    override fun deserialize(decoder: Decoder): ViewFavoriteQuestionScreenParameters {
        return Json.decodeFromString<ViewFavoriteQuestionScreenParameters>(decoder.decodeString())

    }

    override fun serialize(encoder: Encoder, value: ViewFavoriteQuestionScreenParameters) {
        encoder.encodeString(Json.encodeToString(value))
    }
}

@Serializable
object SettingsScreen
