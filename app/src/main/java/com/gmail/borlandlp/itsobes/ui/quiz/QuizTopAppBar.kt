package com.gmail.borlandlp.itsobes.ui.quiz

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.constraintlayout.compose.ConstraintLayout
import com.gmail.borlandlp.itsobes.R
import com.gmail.borlandlp.itsobes.ui.theme.NavColor
import com.gmail.borlandlp.itsobes.ui.theme.White


@Composable
@Preview
fun QuizTopAppBar(
    currentQuestion: Int = 0,
    totalQuestions: Int = 10,
    navigateUp: () -> Unit = {},
    isBlacklisted: Boolean = false,
    blacklistClicked: () -> Unit = {},
) {
    ConstraintLayout(
        modifier = Modifier
            .fillMaxWidth()
            .background(NavColor)
            .padding(top = 10.dp, bottom = 10.dp),
    ) {
        val (image, text, blockBtn) = createRefs()

        Image(
            painter = painterResource(id = R.drawable.ic_arrow_left_white),
            contentDescription = "Navigation up button",
            modifier = Modifier
                .clickable { navigateUp() }
                .constrainAs(image) {
                    start.linkTo(parent.start, margin = 29.dp)
                    top.linkTo(parent.top)
                    bottom.linkTo(parent.bottom)
                }
                .height(14.dp)
                .width(8.dp),
        )

        Text(
            text = stringResource(R.string.question_title, currentQuestion, totalQuestions),
            color = White,
            style = MaterialTheme.typography.titleLarge,
            maxLines = 1,
            overflow = TextOverflow.Ellipsis,
            modifier = Modifier
                .constrainAs(text) {
                    start.linkTo(image.end, 29.dp)
                },
        )

        val blackListIcon = if (!isBlacklisted) {
            painterResource(id = R.drawable.ic_block)
        } else {
            painterResource(id = R.drawable.ic_return)
        }
        Image(
            painter = blackListIcon,
            contentDescription = "Blacklist question button",
            modifier = Modifier
                .height(23.dp)
                .width(23.dp)
                .constrainAs(blockBtn) {
                    end.linkTo(parent.end, 29.dp)
                    top.linkTo(parent.top)
                    bottom.linkTo(parent.bottom)
                }
                .clickable {
                    blacklistClicked()
                },
        )
    }
}