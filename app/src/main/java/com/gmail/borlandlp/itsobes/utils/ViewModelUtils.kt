package com.gmail.borlandlp.itsobes.utils

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.flow.launchIn

fun <T> Flow<T>.inIO(): Flow<T> = flowOn(Dispatchers.IO)

fun <T> Flow<T>.inViewModel(viewModelScope: CoroutineScope): Job = launchIn(viewModelScope)