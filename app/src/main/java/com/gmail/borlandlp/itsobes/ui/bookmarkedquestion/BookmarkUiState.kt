package com.gmail.borlandlp.itsobes.ui.bookmarkedquestion

import android.os.Parcel
import android.os.Parcelable

class BookmarkUiState(
    val title: String = "",
    val tags: List<String> = emptyList(),
    val content: String = "",
    val link: String = "",
) : Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readString()!!,
        parcel.createStringArrayList()!!,
        parcel.readString()!!,
        parcel.readString()!!,
    ) {}

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(title)
        parcel.writeStringList(tags)
        parcel.writeString(content)
        parcel.writeString(link)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<BookmarkUiState> {
        override fun createFromParcel(parcel: Parcel): BookmarkUiState {
            return BookmarkUiState(parcel)
        }

        override fun newArray(size: Int): Array<BookmarkUiState?> {
            return arrayOfNulls(size)
        }
    }
}
