package com.gmail.borlandlp.itsobes.dagger.module

import android.content.Context
import android.content.SharedPreferences
import com.gmail.borlandlp.domain.quizpreferred.PreferredQuizSettingsStorage
import com.gmail.borlandlp.data.quizpreferred.ImplPreferredSettingsQuizStorage
import com.gmail.borlandlp.data.quizpreferred.local.ImplLocalQuizSettingsStorage
import com.gmail.borlandlp.data.quizpreferred.local.LocalQuizSettingsStorage
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ViewModelComponent
import dagger.hilt.android.qualifiers.ApplicationContext
import javax.inject.Named

@Module
@InstallIn(ViewModelComponent::class)
class PreferredSettingsModule {
    @Provides
    fun provideSettings(
        localQuizSettingsStorage: LocalQuizSettingsStorage,
    ): PreferredQuizSettingsStorage {
        return ImplPreferredSettingsQuizStorage(
            localStorage = localQuizSettingsStorage,
        )
    }

    @Provides
    fun provideLocalStorage(
        @Named("quiz_settings_storage") sharedPreferences: SharedPreferences,
    ): LocalQuizSettingsStorage {
        return ImplLocalQuizSettingsStorage(
            sharedPreferences = sharedPreferences,
        )
    }

    @Named("quiz_settings_storage")
    @Provides
    fun provideSharedPreferences(
        @ApplicationContext context: Context
    ): SharedPreferences {
        return context.getSharedPreferences("quiz_settings_storage", Context.MODE_PRIVATE)
    }
}