package com.gmail.borlandlp.itsobes.service.push

import android.app.Activity
import android.app.NotificationManager
import android.content.Context
import android.os.Build
import androidx.core.app.NotificationManagerCompat
import com.gmail.borlandlp.domain.pushservice.LocalPushService
import javax.inject.Inject


class ComposeLocalPushService @Inject constructor(
    private val activity: Activity,
) : LocalPushService {
    override fun notificationsAreEnabled(): Boolean {
        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val manager = activity.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            if (!manager.areNotificationsEnabled()) {
                return false
            }
            val channels = manager.notificationChannels
            for (channel in channels) {
                if (channel.importance == NotificationManager.IMPORTANCE_NONE) {
                    return false
                }
            }
            true
        } else {
            NotificationManagerCompat.from(activity).areNotificationsEnabled()
        }
    }

    override fun requestEnableNotifications() {}
}