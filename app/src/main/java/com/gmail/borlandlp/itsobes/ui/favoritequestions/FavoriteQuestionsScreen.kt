package com.gmail.borlandlp.itsobes.ui.favoritequestions

import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.itemsIndexed
import androidx.compose.material3.Scaffold
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import com.gmail.borlandlp.itsobes.R
import com.gmail.borlandlp.itsobes.ui.EmptyDataScreenUi
import com.gmail.borlandlp.itsobes.ui.NoConnectionViewCompose
import com.gmail.borlandlp.itsobes.ui.ProgressComposeView
import com.gmail.borlandlp.itsobes.ui.actionbar.SimpleActionBarUi
import com.gmail.borlandlp.itsobes.ui.quiz.QuizQuestion
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

@Composable
fun FavoriteQuestionsScreen(
    viewModel: FavoriteQuestionsViewModel = hiltViewModel(),
    navigateToQuestion: (questionUrl: String) -> Unit = {},
) {
    val scope = rememberCoroutineScope()
    LaunchedEffect(true) {
        scope.launch {
            viewModel.navigationEvent.collect {
                when (it) {
                    is NavigationEvent.MoveToQuestion -> {
                        withContext(Dispatchers.Main) {
                            navigateToQuestion(it.questionLink)
                        }
                    }
                }
            }
        }
    }

    if (viewModel.uiState.pageIsLoading) {
        ProgressComposeView(
            text = stringResource(R.string.load_favorite_questions),
        )
    } else if (viewModel.uiState.networkError) {
        NoConnectionViewCompose(
            onBtnReloadCLicked = { viewModel.getFavoriteQuestions() },
        )
    } else if (viewModel.uiState.bookmarksUiState.isEmpty()) {
        EmptyDataScreenUi()
    } else {
        val basePadding = 25.dp
        Scaffold(
            topBar = {
                SimpleActionBarUi(
                    title = stringResource(R.string.bookmarked_questions),
                )
            },
        ) { paddingValues ->
            LazyColumn(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(start = basePadding, end = basePadding)
                    .padding(paddingValues = paddingValues),
            ) {
                itemsIndexed(viewModel.uiState.bookmarksUiState) { index, state ->
                    if (index == 0) {
                        Spacer(modifier = Modifier
                            .fillMaxWidth()
                            .height(basePadding))
                    }

                    QuizQuestion(
                        question = state.title,
                        onClick = {
                            navigateToQuestion(state.link)
                        },
                        isBookmarked = state.bookmarked,
                        bookmarkClicked = {
                            state.bookmarked = !state.bookmarked
                            viewModel.switchBookmarkStatus(state.link)
                        },
                    )

                    Spacer(
                        modifier = Modifier
                        .fillMaxWidth()
                        .height(basePadding),
                    )
                }
            }
        }
    }
}