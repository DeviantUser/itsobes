package com.gmail.borlandlp.itsobes.ui.settings

import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.ExperimentalLayoutApi
import androidx.compose.foundation.layout.FlowRow
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material.ripple.rememberRipple
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.gmail.borlandlp.domain.pushservice.DayOfWeek
import com.gmail.borlandlp.itsobes.ui.theme.PrimaryColor
import com.gmail.borlandlp.itsobes.ui.theme.TypographyPrimary
import com.gmail.borlandlp.itsobes.ui.theme.White
import com.gmail.borlandlp.itsobes.ui.theme.White40percent
import com.gmail.borlandlp.itsobes.utils.defaultBottomShadow

@OptIn(ExperimentalLayoutApi::class)
@Composable
@Preview
fun HorizontalChipGroup(
    modifier: Modifier = Modifier,
    items: List<PushDayOfWeekUi> = emptyList(),
    onStateChanged: (PushDayOfWeekUi) -> Unit = {},
) {
    FlowRow(
        modifier = modifier,
        horizontalArrangement = Arrangement.SpaceAround,
    ) {
        val shape = CircleShape
        items.forEach {
            var selected by remember {
                mutableStateOf(it.enabled)
            }

            Text(
                text = getNameOfDay(it.day),
                color = if (!selected) TypographyPrimary else White,
                modifier = Modifier
                    .clickable (
                        interactionSource = remember { MutableInteractionSource() },
                        indication = rememberRipple(radius = 24.dp, bounded = false),
                        onClick = {
                            selected = !selected
                            it.enabled = selected
                            onStateChanged(it)
                        }
                    )
                    .then(
                        if (!selected) {
                            Modifier
                                .defaultBottomShadow(shape)
                                .background(Color(0xFFDBC3A1))
                                .padding(1.dp)
                                .clip(shape)
                                .background(White40percent)
                                .padding(start = 11.dp, top = 9.dp, end = 11.dp, bottom = 9.dp)
                        } else {
                            Modifier
                                .clip(shape)
                                .background(PrimaryColor)
                                .padding(start = 12.dp, top = 10.dp, end = 12.dp, bottom = 10.dp)
                        }
                    ),
            )
        }
    }
}

private fun getNameOfDay(day: DayOfWeek): String {
    return when(day) {
        DayOfWeek.Monday -> "Пн"
        DayOfWeek.Tuesday -> "Вт"
        DayOfWeek.Wednesday -> "Ср"
        DayOfWeek.Thursday -> "Чт"
        DayOfWeek.Friday -> "Пт"
        DayOfWeek.Saturday -> "Сб"
        DayOfWeek.Sunday -> "Вс"
    }
}
