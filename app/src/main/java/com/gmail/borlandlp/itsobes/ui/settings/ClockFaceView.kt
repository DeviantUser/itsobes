package com.gmail.borlandlp.itsobes.ui.settings

import android.app.TimePickerDialog
import android.app.TimePickerDialog.OnTimeSetListener
import android.content.Context
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableIntStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.constraintlayout.compose.ConstraintLayout
import com.gmail.borlandlp.itsobes.ui.theme.NavColor
import com.gmail.borlandlp.itsobes.ui.theme.TypographyPrimary
import com.gmail.borlandlp.itsobes.ui.theme.White60percent
import com.gmail.borlandlp.itsobes.ui.theme.White90percent


@Composable
@Preview
fun ClockFaceView(
    modifier: Modifier = Modifier,
    initialMinutes: Int = 59,
    initialHours: Int = 23,
    context: Context = LocalContext.current,
    isDisabled: Boolean = false,
    stateChanged: (newHours: Int, newMinutes: Int) -> Unit = { _, _ -> },
) {
    var minutesState by remember {
        mutableIntStateOf(initialMinutes)
    }
    var hoursState by remember {
        mutableIntStateOf(initialHours)
    }
    ConstraintLayout {
        val (content, blocker) = createRefs()

        Row(
            verticalAlignment = Alignment.CenterVertically,
            modifier = modifier
                .constrainAs(content) {
                    start.linkTo(parent.start)
                    top.linkTo(parent.top)
                }
                .clickable {
                    showTimePicker(
                        context,
                        {_, mHour : Int, mMinute: Int ->
                            hoursState = mHour
                            minutesState = mMinute
                            stateChanged(hoursState, minutesState)
                        },
                        hoursState,
                        minutesState,
                    )
                },
        ) {
            val defaultPadding = 8.dp
            val shape = RoundedCornerShape(8.dp)
            val baseFontSize = 24.sp
            val bgColor = if (isDisabled) {
                White90percent
            } else {
                White60percent
            }
            val fontColor = if (isDisabled) { TypographyPrimary } else { NavColor }

            Text(
                text = if (hoursState < 10) { "0$hoursState" } else hoursState.toString(),
                color = fontColor,
                modifier = Modifier
                    .clip(shape)
                    .background(White60percent)
                    .padding(defaultPadding),
                fontSize = baseFontSize,
            )

            Text(
                text = ":",
                color = fontColor,
                modifier = Modifier.padding(start = 4.dp, end = 4.dp),
                fontSize = baseFontSize,
            )

            Text(
                text = if (minutesState < 10) { "0$minutesState" } else minutesState.toString(),
                color = fontColor,
                modifier = Modifier
                    .clip(shape)
                    .background(White60percent)
                    .padding(defaultPadding),
                fontSize = baseFontSize,
            )
        }

//        if (isDisabled) {
//            Box(modifier = Modifier
//                .background(White40percent)
//                .constrainAs(blocker) {
//                    start.linkTo(content.start)
//                    top.linkTo(content.top)
//                    end.linkTo(content.end)
//                    bottom.linkTo(content.bottom)
//                    width = Dimension.fillToConstraints
//                    height = Dimension.fillToConstraints
//                },
//            )
//        }
    }
}

private fun showTimePicker(
    context: Context,
    listener: OnTimeSetListener,
    hourOfDay: Int,
    minute: Int,
    is24HourView: Boolean = true
) {
    TimePickerDialog(
        context,
        listener,
        hourOfDay,
        minute,
        is24HourView,
    ).show()
}

