package com.gmail.borlandlp.itsobes.ui.settings

import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewModelScope
import com.gmail.borlandlp.domain.pushservice.AddPushCompanyResult
import com.gmail.borlandlp.domain.pushservice.ChangePushStateResult
import com.gmail.borlandlp.domain.pushservice.DayOfWeek
import com.gmail.borlandlp.domain.pushservice.DisablePushNotificationsUseCase
import com.gmail.borlandlp.domain.pushservice.EnablePushNotificationsUseCase
import com.gmail.borlandlp.domain.pushservice.GetPushSettingsUseCase
import com.gmail.borlandlp.domain.pushservice.GetSettingsResult
import com.gmail.borlandlp.domain.pushservice.PushCompany
import com.gmail.borlandlp.domain.pushservice.PushCompanyService
import com.gmail.borlandlp.domain.pushservice.RemovePushCompanySettingsResult
import com.gmail.borlandlp.domain.pushservice.SetPushCompanySettingsResult
import com.gmail.borlandlp.itsobes.utils.inIO
import com.gmail.borlandlp.itsobes.utils.inViewModel
import com.google.common.collect.ImmutableList
import dagger.assisted.Assisted
import dagger.assisted.AssistedFactory
import dagger.assisted.AssistedInject
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch
import kotlinx.coroutines.plus
import kotlinx.coroutines.withContext

class SettingsViewModel @AssistedInject constructor(
    private val pushCompanyService: PushCompanyService,
    private val coroutineExceptionHandler: CoroutineExceptionHandler,
    private val disablePushNotificationsUseCase: DisablePushNotificationsUseCase,
    @Assisted private val enablePushNotificationsUseCase: EnablePushNotificationsUseCase,
    @Assisted private val getPushSettingsUseCase: GetPushSettingsUseCase,
) : ViewModel() {
    var uiState by mutableStateOf(SettingsUiState())
        private set

    private val _errorMessage = MutableSharedFlow<String>()
    val errorMessage: Flow<String> = _errorMessage
    private val localExceptionHandler by lazy {
        CoroutineExceptionHandler { _, _ ->
            uiState = uiState.copy(
                isLoading = false,
                networkError = true,
            )
        }
    }
    private val baseScope by lazy {
        viewModelScope + SupervisorJob() + coroutineExceptionHandler + localExceptionHandler
    }

    init {
        getSettings()
    }

    fun getSettings() {
        uiState = uiState.copy(
            networkError = false,
            isLoading = true,
        )
        getPushSettingsUseCase().inIO()
            .catch {
                uiState = uiState.copy(
                    isLoading = false,
                    networkError = true,
                )
            }
            .onEach {
                when(it) {
                    is GetSettingsResult.Success -> {
                        withContext(Dispatchers.Main) {
                            uiState = uiState.copy(
                                isLoading = false,
                                data = it.toPushUiState(),
                                enabled = it.enabled,
                            )
                        }
                    }
                    is GetSettingsResult.Error -> {
                        withContext(Dispatchers.Main) {
                            uiState = uiState.copy(
                                isLoading = false,
                            )
                        }
                    }
                    is GetSettingsResult.NoConnectionError -> {
                        uiState = uiState.copy(networkError = true)
                    }
                }
            }
            .inViewModel(viewModelScope)
    }

    fun changeNotificationsState(newState: Boolean) {
        if (newState) {
            uiState = uiState.copy(
                enabled = true,
            )
            enablePushNotificationsUseCase().inIO()
                .onEach {
                    if (it is ChangePushStateResult.Error) {
                        _errorMessage.emit(it.message ?: "При включении настроек уведомлений произошла ошибка")
                        uiState = uiState.copy(
                            enabled = false,
                        )
                    } else if (it is ChangePushStateResult.RequestedEnableNotifications) {
                        uiState = uiState.copy(
                            enabled = false,
                            needOpenRequestPushPermissions = true,
                        )
                    }
                }
                .catch {
                    _errorMessage.emit("При включении настроек уведомлений произошла ошибка")
                    uiState = uiState.copy(
                        enabled = !uiState.enabled
                    )
                }
                .inViewModel(viewModelScope)
        } else {
            disablePushNotificationsUseCase().inIO()
                .onEach {
                    if (it is ChangePushStateResult.Error) {
                        _errorMessage.emit(it.message ?: "При включении настроек уведомлений произошла ошибка")
                    }
                }
                .catch {
                    _errorMessage.emit("При выключении настроек уведомлений произошла ошибка")
                    uiState = uiState.copy(
                        enabled = !uiState.enabled
                    )
                }
                .inViewModel(viewModelScope)
        }
    }

    fun addNewPushCompany() {
        uiState = uiState.copy(
            networkError = false,
            isLoading = true,
        )
        baseScope.launch(Dispatchers.IO) {
            when(val result = pushCompanyService.create()) {
                is AddPushCompanyResult.Success -> {
                    withContext(Dispatchers.Main) {
                        uiState = uiState.copy(
                            networkError = false,
                            isLoading = false,
                            data = ImmutableList.copyOf((uiState.data union listOf(result.data.toState())).toList())
                        )
                    }
                }
                is AddPushCompanyResult.Error -> {
                    withContext(Dispatchers.Main) {
                        uiState = uiState.copy(
                            networkError = false,
                            isLoading = false,
                        )
                        result.message?.let { _errorMessage.emit(it) }
                    }
                }
                is AddPushCompanyResult.NoConnectionError -> {

                }
            }
        }
    }

    fun setPushForDate(campaignState: PushForDateUiState) {
        baseScope.launch(Dispatchers.IO) {
            when(val result = pushCompanyService.put(campaignState.toDomain())) {
                is SetPushCompanySettingsResult.Success -> {}
                is SetPushCompanySettingsResult.Error -> {
                    withContext(Dispatchers.Main) {
                        result.message?.let { _errorMessage.emit(it) }
                    }
                }
                is SetPushCompanySettingsResult.NoConnectionError -> {

                }
            }
        }
    }

    fun removePushCampaign(campaignState: PushForDateUiState) {
        uiState = uiState.copy(
            networkError = false,
            isLoading = true,
        )
        baseScope.launch(Dispatchers.IO) {
            when(val result = pushCompanyService.remove(pushCompanyId = campaignState.campaignId)) {
                is RemovePushCompanySettingsResult.Success -> {
                    withContext(Dispatchers.Main) {
                        uiState = uiState.copy(
                            networkError = false,
                            isLoading = false,
                            data = ImmutableList.copyOf(uiState.data.filter { it.campaignId != campaignState.campaignId })
                        )
                    }
                }
                is RemovePushCompanySettingsResult.Error -> {
                    withContext(Dispatchers.Main) {
                        uiState = uiState.copy(
                            networkError = false,
                            isLoading = false,
                        )
                        result.message?.let { _errorMessage.emit(it) }
                    }
                }
                is RemovePushCompanySettingsResult.NoConnectionError -> {

                }
            }
        }
    }

    fun onDismissPushWindow() {
        uiState = uiState.copy(
            needOpenRequestPushPermissions = false,
        )
    }

    @AssistedFactory
    interface ArticlesFeedViewModelFactory {
        fun create(
            enablePushNotificationsUseCase: EnablePushNotificationsUseCase,
            getPushSettingsUseCase: GetPushSettingsUseCase,
        ): SettingsViewModel
    }

    @Suppress("UNCHECKED_CAST")
    companion object {
        fun providesFactory(
            assistedFactory: ArticlesFeedViewModelFactory,
            enablePushNotificationsUseCase: EnablePushNotificationsUseCase,
            getPushSettingsUseCase: GetPushSettingsUseCase,
        ): ViewModelProvider.Factory = object : ViewModelProvider.Factory {
            override fun <T : ViewModel> create(modelClass: Class<T>): T {
                return assistedFactory.create(
                    enablePushNotificationsUseCase = enablePushNotificationsUseCase,
                    getPushSettingsUseCase = getPushSettingsUseCase,
                ) as T
            }
        }
    }
}

private fun GetSettingsResult.Success.toPushUiState(): ImmutableList<PushForDateUiState> {
    return ImmutableList.copyOf(this.data.map { company ->
        company.toState()
    })
}

private fun PushCompany.toState(): PushForDateUiState {
    return PushForDateUiState(
        hours = this.hours,
        minutes = this.minutes,
        enabled = this.state,
        state = this.daysState.toUiState(),
        campaignId = this.id,
    )
}

private fun Map<DayOfWeek, Boolean>.toUiState(): List<PushDayOfWeekUi> {
    return this.map {
        PushDayOfWeekUi(day = it.key, enabled = it.value)
    }
}

private fun PushForDateUiState.toDomain(): PushCompany {
    return PushCompany(
        id = this.campaignId,
        state = this.enabled,
        hours = this.hours,
        minutes = this.minutes,
        daysState = this.state.toDomainState(),
    )
}

private fun List<PushDayOfWeekUi>.toDomainState(): Map<DayOfWeek, Boolean> {
    val map = mutableMapOf<DayOfWeek, Boolean>()
    this.forEach {
        map[it.day] = it.enabled
    }
    return map
}
