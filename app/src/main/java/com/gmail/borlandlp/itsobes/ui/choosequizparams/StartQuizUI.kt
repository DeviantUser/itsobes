package com.gmail.borlandlp.itsobes.ui.choosequizparams

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.ExperimentalLayoutApi
import androidx.compose.foundation.layout.FlowRow
import androidx.compose.foundation.layout.padding
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp

@OptIn(ExperimentalLayoutApi::class)
@Composable
@Preview
fun TagsGroup(
    modifier: Modifier = Modifier,
    tagUiState: List<TagUiState> = emptyList(),
    onStateChanged: (newTagState: TagUiState) -> Unit = {},
) {
    var counter = 0
    FlowRow(
        modifier = modifier.padding(top = 8.dp),
        horizontalArrangement = Arrangement.spacedBy(8.dp),
        verticalArrangement = Arrangement.spacedBy(8.dp),
    ) {
        repeat(tagUiState.size) {
            Tag(
                tag = tagUiState[counter++],
                onStateChanged = {
                    onStateChanged(it)
                },
            )
        }
    }
}
