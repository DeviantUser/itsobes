package com.gmail.borlandlp.itsobes.ui.favoritequestions

import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.gmail.borlandlp.domain.bookmarkquestion.BookmarkQuestionInfo
import com.gmail.borlandlp.domain.bookmarkquestion.GetBookmarksListInteractor
import com.gmail.borlandlp.domain.bookmarkquestion.SwitchBookmarkResult
import com.gmail.borlandlp.domain.bookmarkquestion.SwitchBookmarkStatusInteractor
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.SharedFlow
import kotlinx.coroutines.flow.asSharedFlow
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject

@HiltViewModel
class FavoriteQuestionsViewModel @Inject constructor(
    private val getBookmarksListInteractor: GetBookmarksListInteractor,
    private val switchBookmarkStatusInteractor: SwitchBookmarkStatusInteractor,
) : ViewModel() {
    private val _navigationEvent = MutableSharedFlow<NavigationEvent>()
    val navigationEvent: SharedFlow<NavigationEvent> = _navigationEvent.asSharedFlow()
    var uiState by mutableStateOf(FavoriteQuestionsUiState())
        private set

    init {
        getFavoriteQuestions()
    }

    fun getFavoriteQuestions() {
        uiState = uiState.copy(pageIsLoading = true)
        viewModelScope.launch {
            withContext(Dispatchers.IO) {
                val bookmarks = getBookmarksListInteractor.execute()
                withContext(Dispatchers.Main) {
                    uiState = uiState.copy(
                        pageIsLoading = false,
                        bookmarksUiState = bookmarks.toListBookmarkedQuestionUiState(),
                    )
                }
            }
        }
    }

    fun switchBookmarkStatus(url: String) {
        viewModelScope.launch {
            val result = switchBookmarkStatusInteractor.execute(url)
            withContext(Dispatchers.Main) {
                when(result) {
                    is SwitchBookmarkResult.AddedToBookmark, is SwitchBookmarkResult.BookmarkIsRemoved -> {}
                    is SwitchBookmarkResult.Error -> {
                        uiState.bookmarksUiState.forEach {
                            if (it.link == url) {
                                it.bookmarked = !it.bookmarked
                            }
                        }
                        uiState = uiState.copy(
                            bookmarksUiState = uiState.bookmarksUiState,
                        )
                    }
                }
            }
        }
    }
}

private fun List<BookmarkQuestionInfo>.toListBookmarkedQuestionUiState(): List<BookmarkedQuestionUiState> {
    return this.map {
        BookmarkedQuestionUiState(
            link = it.link,
            title = it.title,
            markUnixTime = it.addUnixTime,
            bookmarked = true,
        )
    }
}
