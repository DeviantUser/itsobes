package com.gmail.borlandlp.itsobes.ui.quiz

import com.gmail.borlandlp.domain.quiz.QuestionUiState

data class QuizPageState(
    val questionState: QuestionUiState = QuestionUiState(),
    val isLoading: Boolean = false,
    val isError: Boolean = false,
    val errorMessage: String = "",
    val networkError: Boolean = false,
    val isShowingAnswer: Boolean = false,
    val isDisliked: Boolean = false,
    val isBookmarked: Boolean = false,
)