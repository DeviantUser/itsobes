package com.gmail.borlandlp.itsobes.ui.navigation

import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Scaffold
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.shadow
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.navigation.NavType
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.currentBackStackEntryAsState
import androidx.navigation.compose.rememberNavController
import androidx.navigation.navArgument
import com.gmail.borlandlp.itsobes.ui.bookmarkedquestion.FavoriteQuestionScreen
import com.gmail.borlandlp.itsobes.ui.choosequizparams.StartQuizScreen
import com.gmail.borlandlp.itsobes.ui.favoritequestions.FavoriteQuestionsScreen
import com.gmail.borlandlp.itsobes.ui.quiz.QuizScreen
import com.gmail.borlandlp.itsobes.ui.settings.SettingsScreenFactory
import com.gmail.borlandlp.itsobes.ui.splash.SplashScreen

@Composable
@Preview
fun AppNavigation(
    startDestination: String = AppDestinations.QUIZ_PARAMS_SCREEN,
    routes: AppDestinations = AppDestinations,
) {
    val navController = rememberNavController()
    val actions = remember(navController) {
        AppActions(navController, routes)
    }

    Scaffold(
        bottomBar = {
            Row(
                modifier = Modifier
                    .shadow(elevation = 1.dp)
                    .padding(top = 14.dp, bottom = 12.dp)
                    .fillMaxWidth(),
            ) {
                val navBackStackEntry by navController.currentBackStackEntryAsState()
                val currentRoute = navBackStackEntry?.destination?.route
                QuizNavigationItem(
                    selected = currentRoute == AppDestinations.QUIZ_PARAMS_SCREEN,
                    onClick = {
                        if (currentRoute == AppDestinations.QUIZ_PARAMS_SCREEN) return@QuizNavigationItem
                        actions.navigateToMain(false)
                    },
                    modifier = Modifier
                        .fillMaxWidth()
                        .weight(1f),
                )
                FavoriteQuestionsNavigationItem(
                    selected = currentRoute == AppDestinations.FAVORITE_QUESTIONS_SCREEN,
                    onClick = {
                        if (currentRoute == AppDestinations.FAVORITE_QUESTIONS_SCREEN) return@FavoriteQuestionsNavigationItem
                        actions.navigateToFavoriteQuestions()
                    },
                    modifier = Modifier
                        .fillMaxWidth()
                        .weight(1f),
                )
                SettingsNavigationItem(
                    selected = currentRoute == AppDestinations.SETTINGS_SCREEN,
                    onClick = {
                        if (currentRoute == AppDestinations.SETTINGS_SCREEN) return@SettingsNavigationItem
                        actions.navigateToSettings()
                    },
                    modifier = Modifier
                        .fillMaxWidth()
                        .weight(1f),
                )
            }
        }
    ) { paddingValues ->
        NavHost(
            navController = navController,
            startDestination = startDestination,
            modifier = Modifier.padding(paddingValues = paddingValues),
        ) {
            composable(AppDestinations.SPLASH_SCREEN) {
                SplashScreen()
            }

            composable(
                AppDestinations.QUIZ_PARAMS_SCREEN
            ) {
                StartQuizScreen(
                    navigateToQuiz = actions.navigateToQuiz,
                )
            }

            composable(
                route = "${AppDestinations.QUIZ_ROUTE_SCREEN}/{${routes.QUIZ_PARAM_ID}}/{${routes.QUIZ_PARAM_QUESTION_NUM}}",
                arguments = listOf(
                    navArgument(routes.QUIZ_PARAM_ID) { type = NavType.IntType },
                    navArgument(routes.QUIZ_PARAM_QUESTION_NUM) { type = NavType.IntType },
                )
            ) {
                QuizScreen(
                    navigateToNextPageQuiz = actions.navigateToQuizPage,
                    navigateUp = actions.navigateUp,
                    navigateToMain = {
                        actions.navigateToMain(true)
                    },
                )
            }

            composable(
                route = AppDestinations.FAVORITE_QUESTIONS_SCREEN,
            ) {
                FavoriteQuestionsScreen(
                    navigateToQuestion = { questionUrl ->
                        actions.navigateToFavorite(questionUrl)
                    },
                )
            }

            composable(
                route = "${AppDestinations.FAVORITE_QUESTION_SCREEN}/{${AppDestinations.FAVORITE_QUESTION_PARAM_QUESTION_URL}}",
                arguments = listOf(
                    navArgument(AppDestinations.FAVORITE_QUESTION_PARAM_QUESTION_URL) {
                        type = NavType.StringType
                    },
                ),
            ) {
                FavoriteQuestionScreen(
                    navigateUp = actions.navigateUp,
                )
            }

            composable(
                route = AppDestinations.SETTINGS_SCREEN
            ) {
                SettingsScreenFactory().Content()
            }
        }
    }
}