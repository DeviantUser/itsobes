package com.gmail.borlandlp.itsobes.dagger.module

import com.gmail.borlandlp.data.answers.CacheAnswersDao
import com.gmail.borlandlp.data.answers.ImplAnswersRepository
import com.gmail.borlandlp.data.questons.local.QuizDatabase
import com.gmail.borlandlp.data.questons.remote.itsobes.ItSobesHtmlSource
import com.gmail.borlandlp.data.questons.remote.refactoringguru.GuruSource
import com.gmail.borlandlp.domain.answer.AnswersRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ViewModelComponent

@Module(includes = [QuizModule::class])
@InstallIn(ViewModelComponent::class)
class AnswersModule {
    @Provides
    fun provideSource(
        answersDao: CacheAnswersDao,
        htmlSource: ItSobesHtmlSource,
        guruSource: GuruSource,
    ): AnswersRepository {
        return ImplAnswersRepository(
            sources = listOf(
                com.gmail.borlandlp.data.answers.AnswersCacheStorage(answersDao, htmlSource),
                com.gmail.borlandlp.data.answers.AnswersCacheStorage(answersDao, guruSource),
            ),
        )
    }

    @Provides
    fun getCacheAnswersDao(db: QuizDatabase): CacheAnswersDao {
        return db.getAnswersDao()
    }
}