package com.gmail.borlandlp.itsobes.ui.choosequizparams

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.BasicTextField
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material3.LocalTextStyle
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.text.TextRange
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.input.TextFieldValue
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.gmail.borlandlp.itsobes.ui.theme.TypographyPrimary
import com.gmail.borlandlp.itsobes.ui.theme.White
import com.gmail.borlandlp.itsobes.utils.defaultBottomShadow
import kotlin.math.max
import kotlin.math.min


@Preview
@Composable
fun BorderedTextField(
    value: String = "5",
    onValueChange: (Int) -> Unit = {},
) {
    var textFieldValueState by remember {
        mutableStateOf(
            TextFieldValue(
                text = value,
                selection = TextRange(value.length)
            )
        )
    }

    val shape = RoundedCornerShape(50.dp)
    BasicTextField(
        modifier = Modifier
            .defaultBottomShadow(shape)
            .clip(shape)
            .background(TypographyPrimary)
            .padding(1.dp)
            .clip(shape)
            .background(White)
            .padding(start = 10.dp, end = 10.dp, bottom = 4.dp, top = 4.dp)
            .width(215.dp),
        value = textFieldValueState,
        onValueChange = {
            val filteredValue = it.text.replace(regex = Regex("\\D"), replacement = "")
            val filteredValueInt = min(50, max(0, if (filteredValue.isNotEmpty()) filteredValue.toInt() else 0))

            val filteredValueStr = filteredValueInt.toString()
            textFieldValueState = textFieldValueState.copy(
                text = filteredValueStr,
                selection = TextRange(filteredValueStr.length),
            )

            onValueChange(filteredValueInt)
        },
        keyboardOptions = KeyboardOptions.Default.copy(
            keyboardType = KeyboardType.Number,
        ),
        singleLine = true,
        textStyle = LocalTextStyle.current.copy(
            textAlign = TextAlign.Center,
            color = TypographyPrimary,
        ),
        decorationBox = { innerTextField ->
            innerTextField()
        },
    )
}