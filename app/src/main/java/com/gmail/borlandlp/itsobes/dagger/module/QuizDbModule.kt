package com.gmail.borlandlp.itsobes.dagger.module

import android.app.Application
import androidx.room.Room
import com.gmail.borlandlp.data.questons.local.QuizDatabase
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ViewModelComponent

@Module
@InstallIn(ViewModelComponent::class)
class QuizDbModule {
    @Provides
    fun getDb(
        application: Application,
    ): QuizDatabase {
        return Room.databaseBuilder(
            application,
            QuizDatabase::class.java,
            "QuizDatabase",
        )
        .allowMainThreadQueries()
        .build()
    }
}