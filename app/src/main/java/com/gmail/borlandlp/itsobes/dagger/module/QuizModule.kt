package com.gmail.borlandlp.itsobes.dagger.module

import com.gmail.borlandlp.data.questons.local.QuizDatabase
import com.gmail.borlandlp.data.quiz.ImplQuizRepository
import com.gmail.borlandlp.data.quiz.QuizToEntityMapper
import com.gmail.borlandlp.data.quiz.local.LocalStorage
import com.gmail.borlandlp.data.quiz.local.QuizDao
import com.gmail.borlandlp.domain.quiz.QuizRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ViewModelComponent

@Module(includes = [QuizDbModule::class])
@InstallIn(ViewModelComponent::class)
class QuizModule {
    @Provides
    fun getRepository(
        localStorage: LocalStorage,
        mapper: QuizToEntityMapper,
    ): QuizRepository {
        return ImplQuizRepository(
            localStorage,
            mapper,
        )
    }

    @Provides
    fun getQuizDao(db: QuizDatabase): QuizDao {
        return db.getQuizDao()
    }
}